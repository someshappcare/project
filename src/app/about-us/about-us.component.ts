import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestapiService } from '../restapi.service';
import { Images } from 'src/app/constants/images';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {

  constructor(private spinner: NgxSpinnerService, private router: Router, private restservice: RestapiService) { }
  aboutusList: any; testimonialslistloop: any;
  aboutuslist: []; testimonialslist: [];
  blogpath: any;
  aboutUs: [];
  star = Images.star;
  starcolor = Images.starcolor;
  arrowright = Images.arrowright;
  arrowleft = Images.arrowleft;
  aboutuspic = Images.aboutuspic;
  chunks(array, size) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }
  ngOnInit() {
    this.aboutuslistdata();
    this.testimonialsdata();
  };

  aboutuslistdata() {
    this.spinner.show();
    this.restservice.aboutUs().subscribe((list: any) => {
      if (list.status == 200) {
        //console.log(list);
        this.aboutuslist = list.data;
        //console.log(this.aboutuslist);
      }
    }, err => {
      console.log("error message" + err);
    });
  }

  // recentblogslistdata() {
  //   this.restservice.recentBlogs().subscribe((list: any) => {
  //     if (list.status == 200) {
  //       //  console.log(list);
  //       this.spinner.hide();
  //       this.blogpath = list.BlogImagePath;
  //       this.aboutUs = list.data;
  //       //  console.log(this.aboutUs);
  //       this.aboutusList = this.chunks(this.aboutUs, 4);
  //       let last = this.aboutusList[this.aboutusList.length - 1].length;
  //       if (this.aboutusList.length > 1 && last < 4) {
  //         this.aboutusList[this.aboutusList.length - 1] = [...this.aboutusList[this.aboutusList.length - 1], ...this.aboutusList[0].slice(0, 4 - last)];
  //       }
  //     }
  //   }, err => {
  //     console.log("error message" + err);
  //   });
  // }
  testimonialsdata(){
    this.restservice.testimonial().subscribe((list: any) => {
        if (list.status == 200) {
          this.spinner.hide();
         //  console.log(list);
          this.testimonialslist = list.data;
         //  console.log(this.testimonialslist);
         this.testimonialslistloop = this.chunks(this.testimonialslist,3);
         let last2 = this.testimonialslistloop[this.testimonialslistloop.length-1].length;
         if(this.testimonialslistloop.length > 1 && last2 < 3) {
           this.testimonialslistloop[this.testimonialslistloop.length-1] = [ ...this.testimonialslistloop[this.testimonialslistloop.length-1] , ...this.testimonialslistloop[0].slice(0, 3-last2) ];
         }
        }
      }, err => {
        console.log("error message" + err);
      });
 }

}