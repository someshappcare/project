import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Images } from 'src/app/constants/images';
import { Const } from '../constants/const';
import { User } from '../models/user';
import { RestapiService } from '../restapi.service';
import { ToastrService } from 'ngx-toastr';
import { SocialAuthService, GoogleLoginProvider, FacebookLoginProvider, SocialUser } from "angularx-social-login";
import { NgxSpinnerService } from 'ngx-spinner';
import { Sociallog } from '../models/sociallog';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  fieldTextTypePassword: boolean;
  userdata: any;
  left_img = Images.left_image;
  logoIcon = Images.logo_icon;
  emailIcon = Images.email_icon;
  passwordIcon = Images.password_icon;
  googleIcon = Images.google_icon;
  facebookIcon = Images.facebook_icon;
  socialuser: SocialUser;
  passwordValidation = Const.password_validation;
  emailValidation = Const.email_validation;
  emailpatternValidation = Const.emailpattern_validation;
  apikey = Const.apikey;
  error = "";
  user: User = new User();
  sociallog: Sociallog = new Sociallog();
  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  loginForm: FormGroup; forgotForm: FormGroup;
  submitted = false;
  forgotsubmitted= false;
  @ViewChild('closeforgotpasswordmodel') closeforgotpasswordmodel;
  constructor(private spinner: NgxSpinnerService, private socialAuthService: SocialAuthService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService) { }
  ngOnInit() {
    
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      password: ['', [Validators.required]]
    });
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
    });
  }
  get f() { return this.loginForm.controls; }
  get f1() { return this.forgotForm.controls; }

  onSubmit() {
    this.user.Email = this.loginForm.value.email,
      this.user.Password = this.loginForm.value.password,
      this.user['API-KEY'] = this.apikey
    this.submitted = true;
    // console.log(this.loginForm.value);
     console.log(this.user);
    if (this.loginForm.invalid) {
      // console.log(this.loginForm.invalid);
      // console.log("return");
      return;
    }
    this.spinner.show();
    this.restservice.loginuser(this.user).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
        console.log(list);
        this.userdata = list.data;
        // console.log("user details", this.user);
        localStorage.setItem('currentUser', JSON.stringify(this.userdata));
        this.spinner.hide();
        this.router.navigate(['/']);
      } else {
        this.spinner.hide();
        // console.log(list.message);
        this.toastr.error('', list.message);
        // this.submitted = false;
        // this.loginForm.reset();
      }
    }, err => {
      console.log("error message" + err);
    });
  }
  onforgotSubmit(){
    this.user.Email = this.forgotForm.value.email,
    this.user['API-KEY'] = this.apikey
  this.forgotsubmitted = true;
  // console.log(this.forgotForm.value);
  // console.log(this.user);
  if (this.forgotForm.invalid) {
    // console.log(this.forgotForm.invalid);
    // console.log("return");
    return;
  } this.spinner.show();
  this.restservice.forgotPassword(this.user).subscribe((list: any) => {
    // console.log(list.status);
    if (list.status == 200) {
      console.log(list);
      this.toastr.success('', list.message);
      this.closeforgotpasswordmodel.nativeElement.click();
      this.spinner.hide();
    } else {
      this.spinner.hide();
       console.log(list);
      this.toastr.error('', list.message);
      // this.forgotsubmitted = false;
      // this.forgotForm.reset();
    }
  });
}
   public socialSignIn(socialPlatform : string) {
    let socialPlatformProvider;
    if(socialPlatform == "facebook"){
      socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
    }else if(socialPlatform == "google"){
      socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
    } 
  this.socialAuthService.signIn(socialPlatformProvider).then(
    (userData) => {
      // console.log(socialPlatform+" sign in data : " , userData);
      // Now sign-in with userData
      if (userData != null) {
        this.sociallog.email = userData.email,
        this.sociallog.authToken = userData.authToken,
        this.sociallog.name = userData.name,
        this.sociallog.id = userData.id,
        this.sociallog.Username = userData.firstName,
        this.sociallog.photoUrl = userData.photoUrl,
        this.sociallog.provider = userData.provider,
        this.sociallog['API-KEY'] = this.apikey,
        this.spinner.show();
        // console.log("model", this.sociallog);
        this.restservice.googleLogin(this.sociallog).subscribe((list: any) => {
          console.log(list.status);
          if (list.status == 200) {
            // console.log(list);
            // console.log(list.message);
            // console.log(list.data);
            this.userdata = list.data;
            // console.log("user details", this.userdata);
            localStorage.setItem('currentUser', JSON.stringify(this.userdata));
            this.spinner.hide();
            this.router.navigate(['/']);
          } else {
            this.spinner.hide();
            // console.log(list.message);
            this.toastr.error('', list.message);
            this.submitted = false;
          }
        }, err => {
          console.log("error message" + err);
        });
      }
    }
  );
}
}