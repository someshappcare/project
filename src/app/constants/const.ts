export class Const {

  // constants

public static apikey = '827ccb0eea8a706c4c34a16891f84e7b';
public static username_validation = "User Name is required";
public static usernames_validation = "Name is required";
public static password_validation = "Password is required";
public static email_validation = "Email is required";
public static emailpattern_validation = "Email must be a valid";
public static userId_validation = "UserId is required";
public static confirmpassword_validation = "Confirm Password is required";
public static confirmpasswordvalidator_validation = "Password and Confirm Password must match";
public static mobileno_validation = "Mobile No is required";
public static mobilenolength_validation = "Mobile no must be 10 Digits";
public static mobilenodigits_validation = "Mobile no must be numbers";
public static dob_validation = "DOB is required";
public static gender_validation = "Gender is required";
public static country_validation = "Country is required";
public static oldpassword_validation = "Old Password is required";
public static newpassword_validation = "New Password is required";
public static message_validation = "Message is required";
public static degree_validation = "Degree is required";
public static specilization_validation = "Specilization is required";
public static specilization_pattern_validation = "Specilization doesn't accept special characters";
public static startyear_validation = "Start Year is required";
public static endyear_validation = "End Year is required";
public static endyearstartyear_validation = "End year should be greater than Start year";
public static logintotketest = "Please login!";
public static buytotketest = "Please buy the product!";
public static logintoseefooter = "Please login!";
public static logintopayment = "Please login for payment!";
public static nomocktesttobuy = "No mocktests to buy!";
public static clientmobile = "8431010766";
public static clientemail = "connect@treta.info";
public static clientaddress = "P.No 36,37/P, Sai Baba Nagar, Secunderabad Hyd -500010";
public static shareurl = "http://159.65.145.170/treta";
public static subscribed = "Already subscribed!";
public static progress = "Work in progress";
public static noQuestions = "No questions to take test";
public static noSlotsvailabledata = "No timeSlots available for this date";

// constant values

public static zero = '0';
public static one = '1';
public static two = '2';
public static three = '3';
public static four = '4';
public static five = '5';
public static six = '6';
}
