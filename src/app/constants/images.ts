export class Images {

    // common images
    public static logo_icon ="assets/treta-logo-1.png"
    public static logo_icon2 ="assets/treta-logo-2.png" 
    public static rupee= "assets/icons/rupee.png";   
    // login& register images
    public static left_image= "assets/login-sideimage-1.png";
    public static username_icon= "assets/icons/user-1.png";
    public static password_icon= "assets/icons/key-1.png";
    public static email_icon= "assets/icons/email-1.png";
    public static google_icon= "assets/icons/google.png";
    public static facebook_icon= "assets/icons/facebook-1.png";
    public static name_icon= "assets/icons/name.png";
    public static mobile_icon= "assets/icons/call-1.png";
    public static subscribe1= "assets/subscribe-1.png";
    public static arrowwhiteright= "assets/icons/arrow-white-right-1.png";
    public static rightarrowcarousal= "assets/right-arrow-carousal-1.png";
    public static leftarrowcarousal= "assets/left-arrow-carousal-1.png";
    public static sessionsleftarrow= "assets/sessions-left-arrow-1.png";
    public static sessionsrightarrow= "assets/sessions-right-arrow-1.png";
    public static contactpic= "assets/contact-pic.png";    
    

    // home images
    public static search_icon= "assets/icons/search@2x.png";
    public static notification_icon= "assets/icons/notifications.png";
    public static notification_dropdown_icon= "assets/icons/notification.png";
    public static logout_icon= "assets/icons/login-logout.png";
    public static profile_icon= "assets/icons/profile.png";
    public static notsavedproduct= "assets/saved-product-2.png";
    public static savedproduct= "assets/saved-product-1.png";
    public static testimonialsleft= "assets/testimonialsleft.png";
    public static testimonialsright= "assets/testimonialsright.png";
    public static aboutuspic= "assets/aboutus-pic-1.png";
    public static star= "assets/icons/star.png";
    public static starcolor= "assets/icons/starcolor.png";
    

    // product page
    public static mocktestsideimage= "assets/mocktest-sideimage.png"; 
    public static mocktestnumber= "assets/mock-test-number.png";
    public static mocktestshare= "assets/mock-test-share.png";
    public static plus= "assets/icons/plus-1.png";
    public static delete= "assets/icons/delete.png"; 
    public static aboutus= "assets/aboutus-pic-1.png";  
    public static emailfill= "assets/icons/email-fill-2.png";
    public static facebook2= "assets/icons/facebook2.png";
    public static whatsapp2= "assets/icons/whatsapp2.png";
    public static twitter2= "assets/icons/twitter2.png";
    public static sloticon= "assets/icons/sloticon.png";
    
  // side nav images
    public static arrow= "assets/icons/downarrow.png";
    public static arrowup= "assets/icons/dropdownup.png";
    public static privacy= "assets/icons/privacy-1.png"; 
    public static allcourses= "assets/icons/allcourses.png";
    public static purchases= "assets/icons/purchases.png";
    public static profilesetting= "assets/icons/profile-setting.png";
    public static key= "assets/icons/key-11.png";
    public static logout= "assets/icons/logout.png";
    public static help= "assets/icons/help-1.png";
    public static cross= "assets/icons/cross.png";
    
// aboutus nav images
    public static arrowright= "assets/icons/arrow-right-1.png"; 
    public static arrowleft= "assets/icons/arrow-left-1.png";
    public static profileicon= "assets/icons/profileicon.png";

    // report images
    public static reporttick= "assets/icons/reporttick.png";
    public static reportstar= "assets/icons/reportstar.png";
    public static reporttime= "assets/icons/reporttime.png";
    public static reportbluetick= "assets/icons/reportbluetick.png";

    // footer images
   public static phonecall= "assets/icons/phonecall.png";
   public static mail= "assets/icons/mail.png";

   // payment images
    public static dotlines= "assets/icons/dot-lines.png";
}

