import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Passwordmatcher } from 'src/app/constants/passwordmatcher';

import { Router } from '@angular/router';
import { Images } from 'src/app/constants/images';
import { Const } from 'src/app/constants/const';
import { User } from '../models/user';
import { RestapiService } from '../restapi.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  fieldTextTypePassword;
  confirm_text_password;
  error ="";
  left_img = Images.left_image;
  logoIcon = Images.logo_icon;
  usernameIcon = Images.username_icon;
  emailIcon = Images.email_icon;
  passwordIcon = Images.password_icon;
  googleIcon = Images.google_icon;
  facebookIcon = Images.facebook_icon;
  name_icon =Images.name_icon;
  mobile_icon =Images.mobile_icon;
  apikey =Const.apikey;
  usernameValidation = Const.username_validation;
  passwordValidation = Const.password_validation;
  usernamesvalidation =Const.usernames_validation;
  emailValidation = Const.email_validation;
  emailpatternValidation = Const.emailpattern_validation;
  userIdValidation = Const.userId_validation;
  confirmpasswordValidation = Const.confirmpassword_validation;
  confirmpasswordvalidatorValidation = Const.confirmpasswordvalidator_validation;
  mobilenoValidation = Const.mobileno_validation;
  mobilenolengthValidation = Const.mobilenolength_validation;
  mobilenodigitsValidation = Const.mobilenodigits_validation;

  registerForm: FormGroup;
  submitted = false;

  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  toggleConfirmFieldPassword() {
    this.confirm_text_password = !this.confirm_text_password;
  }
  user: User = new User();
  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService,private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      // fullname: ['', [Validators.required]],
      // name: ['', [Validators.required]],
      email: ['', [Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]],
      mobile: ['',[Validators.required, Validators.pattern("^[0-9]*$")]],
      password: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]]
    }
    , {
      validator: Passwordmatcher('password', 'confirm_password')
    });
  }
  get f() { return this.registerForm.controls; }

  onSubmit() {
    // this.user.Username =this.registerForm.value.fullname,
    // this.user.Name =this.registerForm.value.name,
    this.user.Email =this.registerForm.value.email,
    this.user.Mobile =this.registerForm.value.mobile,
    this.user.Password =this.registerForm.value.password,
    this.user['API-KEY'] =this.apikey
 
    this.submitted = true;
    // console.log(this.registerForm.value);
    // console.log(this.user);
    if (this.registerForm.invalid) {
      // console.log(this.registerForm.invalid);
      // console.log("return");
      return;
    }
    // console.log("else");
    // console.log(this.user);
    this.spinner.show();
    this.restservice.registerUser(this.user).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list.message);
        this.toastr.success('', list.message);
        this.spinner.hide();
         this.router.navigate(['login']);
      } else {
        // console.log(list.message);
        this.spinner.hide();
        this.toastr.error('', list.message);
        // this.submitted = false;
        // this.registerForm.reset();
      }
    }, err => {
      console.log("error message");
    });
  }
}