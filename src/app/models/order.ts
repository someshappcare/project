export class Order {
    ProductID: string;
    UserID: string;
    Amount: string;
    PaymentID: string;
    Signature: string;
    OrderID: string;
    "API-KEY" :string;
}
