export class User {
    UserID: string;
    Username :string;
    Name :string;
    Email :string;
    Mobile :string;
    MobileNumber: string;
    Password :string;
    Message: string;
    UGDegreeID: string;
    UGSpecialization: string;
    UGStartYear: string;
    UGEndYear: string;
    PGDegreeID: string;
    PGSpecialization: string;
    PGStartYear: string;
    PGEndYear: string;
    "API-KEY" :string;
}
