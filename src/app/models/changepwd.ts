export class Changepwd {
    UserID: string;
    oldPassword: string;
    newPassword: string;
    confirmPassword: string;
    Email: string;
    "API-KEY" :string;
}
