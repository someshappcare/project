export class Product {
    ProductID: string;
    MockTestID: string;
    QuestionID: number;
    AnswerID: string;
    UserID: string;
    TotalQuestions: string;
    TimeSpent: string;
    UserAttempt: string;
    GrandProduct: String;
    TimeSlotID: String;
    TimeSlot:String;
    SelectedDate: String;
    "API-KEY" :string;
}
