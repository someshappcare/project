"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
exports.RestapiService = void 0;
var http_1 = require("@angular/common/http");
var core_1 = require("@angular/core");
var RestapiService = /** @class */ (function () {
    function RestapiService(http) {
        this.http = http;
        //url = "http://159.65.145.170";
        //url = "http://13.232.233.77";
        this.url = "https://treta.info";
        this.apikey = '827ccb0eea8a706c4c34a16891f84e7b';
        //razorpaykey ='rzp_test_xtk3C7BEZvQ31u';
        this.razorpaykey = 'rzp_live_9fdi9IrwIqUQkt';
        this.login = '/tretaadmin/index.php/api/User/login';
        this.register = '/tretaadmin/index.php/api/User/register';
        this.forgotpassword = '/tretaadmin/api/User/forgetPassword';
        this.googlelogin = '/tretaadmin/api/User/socialLogin';
        this.courseslist = '/tretaadmin/api/Course/list';
        this.productlist = '/tretaadmin/api/Course/productlist';
        this.topsessions = '/tretaadmin/api/Course/topsessions';
        this.userdetailsbyid = '/tretaadmin/api/User/userProfile';
        this.userprofileupdate = '/tretaadmin/api/User/profileUpdate';
        this.productdetailsbyid = '/tretaadmin/api/Course/productdetail';
        this.bookslot = '/tretaadmin/api/Course/bookslot';
        this.mocktestlist = '/tretaadmin/api/Course/mocktestlist';
        this.homestats = '/tretaadmin/api/Page/homestats';
        this.mocktestdetailinstruction = '/tretaadmin/api/Course/mocktestinstruction';
        this.mocktestquestions = '/tretaadmin/api/Course/mocktestdetail';
        this.mocktestnextclick = '/tretaadmin/api/User/submitanswer';
        this.checkanswerselectedornot = '/tretaadmin/api/User/checkanswer';
        this.submitmockTtest = '/tretaadmin/api/User/submitmocktest';
        this.mocktestreport = '/tretaadmin/api/User/mocktestreport';
        this.mocktestperformancereport = '/tretaadmin/api/User/performancereport';
        this.changepassword = '/tretaadmin/api/User/changePassword';
        this.saveproduct = '/tretaadmin/api/User/saveProduct';
        this.removesavedproduct = '/tretaadmin/api/User/removeSavedProduct';
        this.savedproductlist = '/tretaadmin/api/User/savedProductList';
        this.purchaseproduct = '/tretaadmin/api/User/purchasedProductList';
        this.aboutus = '/tretaadmin/api/Page/aboutus';
        this.privacypolicy = '/tretaadmin/api/Page/privacypolicy';
        this.termsandconditions = '/tretaadmin/api/Page/termsCondition';
        this.testimonials = '/tretaadmin/api/Page/testimonials';
        this.faqs = '/tretaadmin/api/Page/faqlist';
        this.features = '/tretaadmin/api/Page/featurelist';
        this.recentblogs = '/tretaadmin/api/Page/recentblogs';
        this.allbloglist = '/tretaadmin/api/Page/bloglist';
        this.contactus = '/tretaadmin/api/Page/contactus';
        this.profiledegreeslist = '/tretaadmin/api/User/degreeList';
        this.specializationlist = '/tretaadmin/api/User/specializationList';
        this.saveeducationdetails = '/tretaadmin/api/User/saveDegree';
        this.userresumeupload = '/tretaadmin/api/User/resumeUpload';
        this.profilephotoupdate = '/tretaadmin/api/User/profilepicUpload';
        this.removesaveddegree = '/tretaadmin/api/User/removeSavedDegree';
        this.createorderid = '/tretaadmin/api/User/createOrder';
        this.verifyorderresponse = '/tretaadmin/api/User/verifyorder';
        this.resetpassword = '/tretaadmin/api/User/resetPassword';
        this.mocktestallquestions = '/tretaadmin/api/Course/mocktestallquestions';
        this.downloadreport = '/tretaadmin/api/Course/downloadreport';
        this.howwework = '/tretaadmin/api/Page/howwework';
        this.refundpolicy = '/tretaadmin/api/Page/refundPolicy';
    }
    RestapiService.prototype.razorPayKey = function () {
        return this.razorpaykey;
    };
    RestapiService.prototype.registerUser = function (registerformdata) {
        return this.http.post(this.url + this.register, registerformdata);
    };
    RestapiService.prototype.loginuser = function (loginformdata) {
        return this.http.post(this.url + this.login, loginformdata);
    };
    RestapiService.prototype.googleLogin = function (googlelogin) {
        return this.http.post(this.url + this.googlelogin, googlelogin);
    };
    RestapiService.prototype.forgotPassword = function (forgotpwd) {
        return this.http.post(this.url + this.forgotpassword, forgotpwd);
    };
    RestapiService.prototype.resetPassword = function (resetPwd) {
        return this.http.post(this.url + this.resetpassword, resetPwd);
    };
    RestapiService.prototype.userDetailsById = function (userdetails) {
        return this.http.post(this.url + this.userdetailsbyid, userdetails);
    };
    RestapiService.prototype.UserProfileUpdate = function (userdetailsupdate) {
        return this.http.post(this.url + this.userprofileupdate, userdetailsupdate);
    };
    RestapiService.prototype.coursesList = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.courseslist, { headers: headers });
    };
    RestapiService.prototype.productsList = function (product) {
        return this.http.post(this.url + this.productlist, product);
    };
    RestapiService.prototype.productDetailsbyId = function (productdetail) {
        return this.http.post(this.url + this.productdetailsbyid, productdetail);
    };
    RestapiService.prototype.bookSlot = function (bookslot) {
        return this.http.post(this.url + this.bookslot, bookslot);
    };
    RestapiService.prototype.mockTestList = function (mocklist) {
        return this.http.post(this.url + this.mocktestlist, mocklist);
    };
    RestapiService.prototype.mockTestDetailInstructions = function (instructions) {
        return this.http.post(this.url + this.mocktestdetailinstruction, instructions);
    };
    RestapiService.prototype.mockTestQuestions = function (exam) {
        return this.http.post(this.url + this.mocktestquestions, exam);
    };
    RestapiService.prototype.mockTestNextClick = function (save) {
        return this.http.post(this.url + this.mocktestnextclick, save);
    };
    RestapiService.prototype.checkAnswerSelectedorNot = function (selectedornot) {
        return this.http.post(this.url + this.checkanswerselectedornot, selectedornot);
    };
    RestapiService.prototype.submitMockTest = function (submit) {
        return this.http.post(this.url + this.submitmockTtest, submit);
    };
    RestapiService.prototype.mockTestReport = function (report) {
        return this.http.post(this.url + this.mocktestreport, report);
    };
    RestapiService.prototype.mockTestPerformanceReport = function (report) {
        return this.http.post(this.url + this.mocktestperformancereport, report);
    };
    RestapiService.prototype.changePassword = function (changepwd) {
        return this.http.post(this.url + this.changepassword, changepwd);
    };
    RestapiService.prototype.saveProduct = function (save) {
        return this.http.post(this.url + this.saveproduct, save);
    };
    RestapiService.prototype.removeSavedProduct = function (unsave) {
        return this.http.post(this.url + this.removesavedproduct, unsave);
    };
    RestapiService.prototype.savedProductList = function (savelist) {
        return this.http.post(this.url + this.savedproductlist, savelist);
    };
    RestapiService.prototype.purchaseProduct = function (purchased) {
        return this.http.post(this.url + this.purchaseproduct, purchased);
    };
    RestapiService.prototype.aboutUs = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.aboutus, { headers: headers });
    };
    RestapiService.prototype.privacyPolicy = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.privacypolicy, { headers: headers });
    };
    RestapiService.prototype.termsAndConditions = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.termsandconditions, { headers: headers });
    };
    RestapiService.prototype.testimonial = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.testimonials, { headers: headers });
    };
    RestapiService.prototype.faq = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.faqs, { headers: headers });
    };
    RestapiService.prototype.refundPolicy = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.refundpolicy, { headers: headers });
    };
    RestapiService.prototype.featureServices = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.features, { headers: headers });
    };
    RestapiService.prototype.recentBlogs = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.recentblogs, { headers: headers });
    };
    RestapiService.prototype.allBlogsList = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.allbloglist, { headers: headers });
    };
    RestapiService.prototype.contactUs = function (contact) {
        return this.http.post(this.url + this.contactus, contact);
    };
    RestapiService.prototype.profileDegreesList = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.profiledegreeslist, { headers: headers });
    };
    RestapiService.prototype.specializationListById = function (degrreid) {
        return this.http.post(this.url + this.specializationlist, degrreid);
    };
    RestapiService.prototype.saveEducationDetails = function (save) {
        return this.http.post(this.url + this.saveeducationdetails, save);
    };
    RestapiService.prototype.userResumeUpload = function (resume) {
        console.log("resumeUpload api");
        return this.http.post(this.url + this.userresumeupload, resume);
    };
    RestapiService.prototype.profilephotoUpdate = function (photo) {
        console.log("profilephotoUpdate api");
        return this.http.post(this.url + this.profilephotoupdate, photo);
    };
    RestapiService.prototype.removeSavedDegree = function (removedegree) {
        return this.http.post(this.url + this.removesaveddegree, removedegree);
    };
    RestapiService.prototype.createOrderId = function (orderid) {
        return this.http.post(this.url + this.createorderid, orderid);
    };
    RestapiService.prototype.verifyOrderResponse = function (verifyorder) {
        return this.http.post(this.url + this.verifyorderresponse, verifyorder);
    };
    RestapiService.prototype.userTopSessions = function (topsession) {
        return this.http.post(this.url + this.topsessions, topsession);
    };
    RestapiService.prototype.mockTestAllQuestions = function (mocktestallquestions) {
        return this.http.post(this.url + this.mocktestallquestions, mocktestallquestions);
    };
    RestapiService.prototype.downloadReport = function (downloadreport) {
        return this.http.post(this.url + this.downloadreport, downloadreport);
    };
    RestapiService.prototype.howWeWork = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.howwework, { headers: headers });
    };
    RestapiService.prototype.homeStats = function () {
        var headers = new http_1.HttpHeaders();
        headers = headers.set('API-KEY', this.apikey);
        return this.http.get(this.url + this.homestats, { headers: headers });
    };
    RestapiService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        })
    ], RestapiService);
    return RestapiService;
}());
exports.RestapiService = RestapiService;
