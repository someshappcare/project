import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BarRatingModule } from "ngx-bar-rating";
import { ToastrModule } from 'ngx-toastr';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocialLoginModule, SocialAuthServiceConfig } from 'angularx-social-login';
import {GoogleLoginProvider,FacebookLoginProvider} from 'angularx-social-login';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { NavBarComponent } from './commonComponents/nav-bar/nav-bar.component';
import { FooterComponent } from './commonComponents/footer/footer.component';
import { ProductComponent } from './products/product/product.component';
import { HttpClientModule } from '@angular/common/http';
import { MocktestComponent } from './products/mocktest/mocktest.component';
import { HomeComponent } from './commonComponents/home/home.component';
import { PaymentComponent } from './payment/payment/payment.component';
import { MocktestlistComponent } from './products/mocktestlist/mocktestlist.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfilesidenavComponent } from './profile/profilesidenav/profilesidenav.component';
import { ProfiledetailsComponent } from './profile/profiledetails/profiledetails.component';
import { ChangepasswordComponent } from './profile/changepassword/changepassword.component';
import { UpdateprofiledetailsComponent } from './profile/updateprofiledetails/updateprofiledetails.component';
import { AllcoursesComponent } from './profile/allcourses/allcourses.component';
import { MypurchasesComponent } from './profile/mypurchases/mypurchases.component';
import { InstructionsComponent } from './products/instructions/instructions.component';
import { FormattimePipe } from './pipes/formattime.pipe';
import { LinebreakPipe } from './pipes/linebreak.pipe';
import { TestComponent } from './products/test/test.component';
import { ReportsComponent } from './products/reports/reports.component';
import { PrivacyComponent } from './profile/privacy/privacy.component';
import { TermsComponent } from './profile/terms/terms.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { QuestionsComponent } from './products/questions/questions.component';
import { OverallperformanceComponent } from './products/overallperformance/overallperformance.component';
import { SpinnerComponent } from './constants/spinner/spinner.component';
import { RefundsComponent } from './commonComponents/refunds/refunds.component';
import { TermsandconditionsComponent } from './commonComponents/termsandconditions/termsandconditions.component';
import { PrivacypolicyComponent } from './commonComponents/privacypolicy/privacypolicy.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    AboutUsComponent,
    ContactUsComponent,
    NavBarComponent,
    FooterComponent,
    ProductComponent,
    MocktestComponent,
    HomeComponent,
    PaymentComponent,
    MocktestlistComponent,
    ProfilesidenavComponent,
    ProfiledetailsComponent,
    ChangepasswordComponent,
    UpdateprofiledetailsComponent,
    AllcoursesComponent,
    MypurchasesComponent,
    InstructionsComponent,
    FormattimePipe,
    LinebreakPipe,
    TestComponent,
    ReportsComponent,
    PrivacyComponent,
    TermsComponent,
    ResetpasswordComponent,
    QuestionsComponent,
    OverallperformanceComponent,
    SpinnerComponent,
    RefundsComponent,
    TermsandconditionsComponent,
    PrivacypolicyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BarRatingModule,
    BrowserAnimationsModule,
    SocialLoginModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    ShareButtonsModule.withConfig({
      debug: true
    }),
    ShareIconsModule 
  ],
  exports: [
    NavBarComponent
],
   providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
               '51635619043-q325hkd0i9ah76nihf77dvj6smoln9lq.apps.googleusercontent.com'
              //'355885190385-qq5p4mg775ojj1lb1l3urv7qk38gp2hn.apps.googleusercontent.com'
            )
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
             provider: new FacebookLoginProvider('492501835094426')
             //provider: new FacebookLoginProvider('646827742752338')
          } 
        ]
      } as SocialAuthServiceConfig,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
