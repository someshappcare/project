import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router } from '@angular/router';
import { Images } from 'src/app/constants/images';
import { Const } from 'src/app/constants/const';
import { User } from '../models/user';
import { RestapiService } from '../restapi.service';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactUsForm: FormGroup;
  submitted = false;
  error = "";
  usernameValidation = Const.username_validation;;
  emailValidation = Const.email_validation;
  emailpatternValidation = Const.emailpattern_validation;
  mobilenoValidation = Const.mobileno_validation;
  mobilenolengthValidation = Const.mobilenolength_validation;
  mobilenodigitsValidation = Const.mobilenodigits_validation;
  messagevalidation = Const.message_validation;
  contactpic =Images.contactpic;
  facebook2 =Images.facebook2;
  whatsapp2 =Images.whatsapp2;
  twitter2 =Images.twitter2;
  mobile_icon = Const.clientmobile;
  clientemail = Const.clientemail;
  clientaddress = Const.clientaddress;
  apikey = Const.apikey;
  user: User = new User();
  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService, private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.contactUsForm = this.formBuilder.group({
      fullname: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      message: ['', [Validators.required]],
    });
  }
  get f() { return this.contactUsForm.controls; }

  onSubmit() {
    this.user.Name = this.contactUsForm.value.fullname,
      this.user.Email = this.contactUsForm.value.email,
      this.user.MobileNumber = this.contactUsForm.value.mobile,
      this.user.Message = this.contactUsForm.value.message,
      this.user['API-KEY'] = this.apikey
    this.submitted = true;
    // console.log(this.contactUsForm.value);
    if (this.contactUsForm.invalid) {
      // console.log(this.contactUsForm.invalid);
      // console.log("return");
      return;
    }
    // console.log(this.user);
    this.spinner.show();
    this.restservice.contactUs(this.user).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list.message);
        this.toastr.success('', list.message);
        this.spinner.hide();
        this.submitted = false;
        this.contactUsForm.reset();
      } else {
        // console.log(list.message);
        this.spinner.hide();
        this.toastr.error('', list.message);
        this.submitted = false;
        this.contactUsForm.reset();
      }
    }, err => {
      console.log("error message");
    });
  }
}
