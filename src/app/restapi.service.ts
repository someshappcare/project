import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RestapiService {
  constructor(private http: HttpClient) { }
   //url = "http://159.65.145.170";
  //url = "http://13.232.233.77";
  url = "https://treta.info";

  apikey = '827ccb0eea8a706c4c34a16891f84e7b';
  //razorpaykey ='rzp_test_xtk3C7BEZvQ31u';
  razorpaykey ='rzp_live_9fdi9IrwIqUQkt';
  razorPayKey(){
    return this.razorpaykey;
  }
  login = '/tretaadmin/index.php/api/User/login';
  register = '/tretaadmin/index.php/api/User/register';
  forgotpassword='/tretaadmin/api/User/forgetPassword';
  googlelogin ='/tretaadmin/api/User/socialLogin';
  courseslist = '/tretaadmin/api/Course/list';
  productlist = '/tretaadmin/api/Course/productlist';
  topsessions ='/tretaadmin/api/Course/topsessions';
  userdetailsbyid = '/tretaadmin/api/User/userProfile';
  userprofileupdate = '/tretaadmin/api/User/profileUpdate';
  productdetailsbyid = '/tretaadmin/api/Course/productdetail';
  bookslot ='/tretaadmin/api/Course/bookslot';
  mocktestlist = '/tretaadmin/api/Course/mocktestlist';
  homestats ='/tretaadmin/api/Page/homestats';
  mocktestdetailinstruction = '/tretaadmin/api/Course/mocktestinstruction';
  mocktestquestions = '/tretaadmin/api/Course/mocktestdetail';
  mocktestnextclick = '/tretaadmin/api/User/submitanswer';
  checkanswerselectedornot = '/tretaadmin/api/User/checkanswer';
  submitmockTtest = '/tretaadmin/api/User/submitmocktest';
  mocktestreport = '/tretaadmin/api/User/mocktestreport';
  mocktestperformancereport = '/tretaadmin/api/User/performancereport';
  changepassword = '/tretaadmin/api/User/changePassword';
  saveproduct = '/tretaadmin/api/User/saveProduct';
  removesavedproduct = '/tretaadmin/api/User/removeSavedProduct';
  savedproductlist = '/tretaadmin/api/User/savedProductList';
  purchaseproduct = '/tretaadmin/api/User/purchasedProductList';
  aboutus = '/tretaadmin/api/Page/aboutus';
  privacypolicy = '/tretaadmin/api/Page/privacypolicy';
  termsandconditions = '/tretaadmin/api/Page/termsCondition';
  testimonials ='/tretaadmin/api/Page/testimonials';
  faqs ='/tretaadmin/api/Page/faqlist';
  features ='/tretaadmin/api/Page/featurelist';
  recentblogs ='/tretaadmin/api/Page/recentblogs';
  allbloglist ='/tretaadmin/api/Page/bloglist';
  contactus ='/tretaadmin/api/Page/contactus';
  profiledegreeslist ='/tretaadmin/api/User/degreeList';
  specializationlist ='/tretaadmin/api/User/specializationList';
  saveeducationdetails ='/tretaadmin/api/User/saveDegree';
  userresumeupload ='/tretaadmin/api/User/resumeUpload';
  profilephotoupdate ='/tretaadmin/api/User/profilepicUpload';
  removesaveddegree ='/tretaadmin/api/User/removeSavedDegree';
  createorderid ='/tretaadmin/api/User/createOrder';
  verifyorderresponse='/tretaadmin/api/User/verifyorder';
  resetpassword ='/tretaadmin/api/User/resetPassword';
  mocktestallquestions ='/tretaadmin/api/Course/mocktestallquestions';
  downloadreport ='/tretaadmin/api/Course/downloadreport';
  howwework ='/tretaadmin/api/Page/howwework';
  refundpolicy ='/tretaadmin/api/Page/refundPolicy';

  registerUser(registerformdata: any) {
    return this.http.post(this.url + this.register, registerformdata);
  }
  loginuser(loginformdata: any) {
    return this.http.post(this.url + this.login, loginformdata);
  }
  googleLogin(googlelogin: any) {
    return this.http.post(this.url + this.googlelogin, googlelogin);
  }
  forgotPassword(forgotpwd: any) {
    return this.http.post(this.url + this.forgotpassword, forgotpwd);
  }
  resetPassword(resetPwd: any) {
    return this.http.post(this.url + this.resetpassword, resetPwd);
  }
  userDetailsById(userdetails: any) {
    return this.http.post(this.url + this.userdetailsbyid, userdetails);
  }
  UserProfileUpdate(userdetailsupdate: any) {
    return this.http.post(this.url + this.userprofileupdate, userdetailsupdate);
  }
  coursesList() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.courseslist, { headers: headers });
  }
  productsList(product: any) {
    return this.http.post(this.url + this.productlist, product);
  }
  productDetailsbyId(productdetail: any) {
    return this.http.post(this.url + this.productdetailsbyid, productdetail);
  }
  bookSlot(bookslot: any) {
    return this.http.post(this.url + this.bookslot, bookslot);
  }
  mockTestList(mocklist: any) {
    return this.http.post(this.url + this.mocktestlist, mocklist);
  }
  mockTestDetailInstructions(instructions: any) {
    return this.http.post(this.url + this.mocktestdetailinstruction, instructions);
  }
  mockTestQuestions(exam: any) {
    return this.http.post(this.url + this.mocktestquestions, exam);
  }
  mockTestNextClick(save: any) {
    return this.http.post(this.url + this.mocktestnextclick, save);
  }
  checkAnswerSelectedorNot(selectedornot: any) {
    return this.http.post(this.url + this.checkanswerselectedornot, selectedornot);
  }
  submitMockTest(submit: any) {
    return this.http.post(this.url + this.submitmockTtest, submit);
  }
  mockTestReport(report: any) {
    return this.http.post(this.url + this.mocktestreport, report);
  }
  mockTestPerformanceReport(report: any) {
    return this.http.post(this.url + this.mocktestperformancereport, report);
  }
  changePassword(changepwd: any) {
    return this.http.post(this.url + this.changepassword, changepwd);
  }
  saveProduct(save: any) {
    return this.http.post(this.url + this.saveproduct, save);
  }
  removeSavedProduct(unsave: any) {
    return this.http.post(this.url + this.removesavedproduct, unsave);
  }
  savedProductList(savelist: any) {
    return this.http.post(this.url + this.savedproductlist, savelist);
  }
  purchaseProduct(purchased: any) {
    return this.http.post(this.url + this.purchaseproduct, purchased);
  }
  aboutUs() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.aboutus, { headers: headers });
  }
  privacyPolicy() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.privacypolicy, { headers: headers });
  }
  termsAndConditions() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.termsandconditions, { headers: headers });
  }
  testimonial() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.testimonials, { headers: headers });
  }
  faq() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.faqs, { headers: headers });
  }

    refundPolicy() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.refundpolicy, { headers: headers });
  }

  featureServices() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.features, { headers: headers });
  }
  recentBlogs() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.recentblogs, { headers: headers });
  }
  allBlogsList() {
    let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.allbloglist, { headers: headers });
  }
 contactUs(contact: any) {
    return this.http.post(this.url + this.contactus, contact);
  }
  profileDegreesList() {
     let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.profiledegreeslist,{ headers: headers });
  }
specializationListById(degrreid: any){
  return this.http.post(this.url + this.specializationlist, degrreid);
}
saveEducationDetails(save: any){
  return this.http.post(this.url + this.saveeducationdetails, save);
}
userResumeUpload(resume: any){
  console.log("resumeUpload api");
  return this.http.post(this.url + this.userresumeupload, resume);
}
profilephotoUpdate(photo: any){
  console.log("profilephotoUpdate api");
  return this.http.post(this.url + this.profilephotoupdate, photo);
}
removeSavedDegree(removedegree: any){
  return this.http.post(this.url + this.removesaveddegree, removedegree);
}
createOrderId(orderid: any){
  return this.http.post(this.url + this.createorderid, orderid);
}
verifyOrderResponse(verifyorder: any){
  return this.http.post(this.url + this.verifyorderresponse, verifyorder);
}
userTopSessions(topsession: any){
  return this.http.post(this.url + this.topsessions, topsession);
}
mockTestAllQuestions(mocktestallquestions: any){
  return this.http.post(this.url + this.mocktestallquestions, mocktestallquestions);
}
downloadReport(downloadreport: any){
  return this.http.post(this.url + this.downloadreport, downloadreport);
}
howWeWork(){
  let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.howwework,{ headers: headers });
}

homeStats(){
  let headers = new HttpHeaders()
    headers = headers.set('API-KEY', this.apikey)
    return this.http.get(this.url + this.homestats,{ headers: headers });
}
}

