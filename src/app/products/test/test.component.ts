import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subscription, timer } from 'rxjs';
import { Const } from 'src/app/constants/const';
import { Product } from 'src/app/models/product';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  id: any; userAttempt: any;
  questions: [];
  product: Product = new Product();
  apikey = Const.apikey;
  testname: any;
  counter: any;
  nextbuttoncheck: boolean;
  tick = 1000;
  totalTimeforexam: any;
  ResumeTime: any;
  countDown: Subscription;
  totolnoofqustns: any;
  currentIndex: number;
  currentQuestionSet: any;
  totalNoOfQuestions: any = [];
  NumQuesAnswered: any;
  NumQuesNotAnswered: any;
  listquestions: [];
  onnextsubmitdata: [];
  answerselectedvalue: any;
  questionselectedvalue: any;
  userData: any;
  userID: any;
  value: string;
  notAnswered: number;
  questionIDincrease: number;
  interval: any;
  zero =Const.zero; one =Const.one; two = Const.two;
  constructor(private spinner: NgxSpinnerService, private locationStrategy: LocationStrategy,private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userID = this.userData[0].UserID;
    }
    this.nextbuttoncheck = true;
    this.route.queryParams.subscribe(
      params => {
        this.id = params.id;
        this.userAttempt = params.userAttempt;
        this.questionslist();
      });
    this.interval =setInterval(() => {
      // console.log(this.counter); 
      if (this.counter == this.zero) {
      clearInterval(this.interval);
      // console.log("clearInterval",this.interval); 
      //  console.log("in loop",this.counter);
      this.testSubmit();
    }
    }, 1000);
    this.preventBackButton();
  }
  preventBackButton() {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
  }
  questionslist() {
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product.UserAttempt = this.userAttempt;
    this.product['API-KEY'] = this.apikey;
     this.spinner.show();
    //  console.log(this.product);
    this.restservice.mockTestQuestions(this.product).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
         this.spinner.hide();
        // console.log(list.message);
        this.questions = list.data[0].QuestAns;
        this.testname = list.data[0].MockTestName;
        this.totalTimeforexam = Math.floor((list.data[0].TotalTime) * 60);
        this.ResumeTime = list.data[0].ResumeTime;
        if(this.ResumeTime == this.totalTimeforexam){
          // console.log("Time",this.ResumeTime, this.totalTimeforexam);
        this.counter = Math.floor((list.data[0].TotalTime) * 60);
        }else{
        this.counter =this.ResumeTime;
        }
        // console.log("ResumeTime",this.ResumeTime);
        // console.log("counter", this.counter);
        this.countDown = timer(0, this.tick).subscribe(() => --this.counter);
        this.totolnoofqustns = list.data[0].TotalQuestions;
        this.notAnswered = list.data[0].TotalQuestions;
        this.NumQuesAnswered = list.data[0].NumQuesAnswered;
        this.NumQuesNotAnswered = list.data[0].NotAnswered;
        // console.log(list.data);
        if (this.currentIndex+1 != this.totolnoofqustns) {
        this.currentIndex = 0;
        this.currentQuestionSet = this.questions[this.currentIndex];
        for (var i = 1; i <= this.questions.length; i++) {
          // console.log(this.currentIndex);
          // console.log("questionIDincrease",this.currentQuestionSet?.QuestionID);
          this.questionIDincrease = this.currentQuestionSet?.QuestionID;
          this.totalNoOfQuestions.push(i);
          // console.log(this.totalNoOfQuestions);
          // console.log("currentQuestionSet",this.currentQuestionSet);
        }
      }
      }
    });
  }

  onItemChange(answervalue: any, questionvalue: any) {
    this.answerselectedvalue = answervalue;
    this.questionselectedvalue = questionvalue;
    //  console.log(" Value is : ", answervalue, questionvalue);
  }
  
  next() {
    // console.log(this.totalTimeforexam);
    let timespent = Math.floor(this.totalTimeforexam - this.counter);
    // console.log(timespent);
    let hours = Math.floor(timespent / 3600);
    timespent %= 3600;
    let minutes = Math.floor(timespent / 60);
    let seconds = timespent % 60;
    // console.log(hours + ":" + minutes + ":" + seconds);
    let finalCounter = hours + ":" + minutes + ":" + seconds;
    // console.log(finalCounter);
    this.product.TimeSpent = finalCounter;
    this.product.UserID = this.userID;
    if(this.questionIDincrease ==this.questionselectedvalue){
    this.product.AnswerID = this.answerselectedvalue;
    }else{
        this.product.AnswerID = "";
    }
    this.product.QuestionID = this.questionIDincrease;
    this.product.UserAttempt = this.userAttempt;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.mockTestNextClick(this.product).subscribe((list: any) => {
      //  console.log(list);
      if (list.status == 200) {
        // console.log(list.message);
        // this.onnextsubmitdata = list.data;
        // console.log(list.data);
       // console.log(this.onnextsubmitdata.NumQuesAnswered,this.onnextsubmitdata.NotAnswered);
        this.NumQuesAnswered = list.data[0].NumQuesAnswered;
        this.NumQuesNotAnswered =  list.data[0].NotAnswered;
      }
    });
    
    if (this.currentIndex+1 != this.totolnoofqustns) {
      // console.log(this.product.QuestionID);
      // console.log(this.totolnoofqustns);
      // console.log(this.currentIndex+1);
      // console.log(this.questionIDincrease);
      this.currentIndex = this.currentIndex + 1;
      this.currentQuestionSet = this.questions[this.currentIndex];
      this.questionIDincrease = this.currentQuestionSet?.QuestionID;
      // console.log(this.currentQuestionSet);
      // console.log(this.currentIndex);
    }else{
      this.nextbuttoncheck = false;
    }
   }
  
  rightsidequstnsclick(rightqustnid: any) {
    // console.log(rightqustnid);
    this.currentIndex = rightqustnid;
    this.currentQuestionSet = this.questions[this.currentIndex];
    this.questionIDincrease = this.currentQuestionSet?.QuestionID;
    // console.log(this.currentQuestionSet);
    this.nextbuttoncheck = true;
  }

  isAnswered(question: any) {
    return question.Answers.find(x => x.AnswerID == question.UserSelectedAnswer) ? 'Answered' : 'Not Answered';
  };

  testSubmit() {
    // console.log(this.counter);
    // console.log(this.totalTimeforexam);
    let timespent = Math.floor(this.totalTimeforexam - this.counter);
    // console.log(timespent);
    let hours = Math.floor(timespent / 3600);
    timespent %= 3600;
    let minutes = Math.floor(timespent / 60);
    let seconds = timespent % 60;
    // console.log(hours + ":" + minutes + ":" + seconds);
    let finalCounter = hours + ":" + minutes + ":" + seconds;
    // console.log(finalCounter);
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product.TimeSpent = finalCounter;
    this.product.TotalQuestions = this.totolnoofqustns;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
     this.spinner.show();
    this.restservice.submitMockTest(this.product).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
         this.spinner.hide();
        // console.log(list.message);
        this.router.navigate(['report'], { queryParams: { id: this.id} });
      }
    });
  }
}
