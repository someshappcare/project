import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  id: any; userAttempt: any;
  product: Product = new Product();
  apikey = Const.apikey;
  userData: any;
  userID: any; ProductID: any;
  testname: any; downloadreportqtns: any;
  questions: [];
  logoIcon = Images.logo_icon;
  currentQuestionSet:any;
  testimonialsright = Images.testimonialsright;
  constructor(private toastr: ToastrService,private spinner: NgxSpinnerService, private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id = params.id;
        this.userAttempt = params.userAttempt;
        // console.log(this.id);
      });
      if (localStorage.getItem('currentUser')) {
        // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
        this.userData = JSON.parse(localStorage.getItem("currentUser"));
        this.userID = this.userData[0].UserID;
      }
      this.Questionslist();
      this.downloadclick();
  }
  Questionslist(){
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product.UserAttempt = this.userAttempt;
    this.product['API-KEY'] = this.apikey;
    //  console.log(this.product);
      this.spinner.show();
    this.restservice.mockTestAllQuestions(this.product).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        // console.log(list.message);
           this.spinner.hide();
        this.testname = list.data[0].MockTestName;
        this.questions = list.data[0].QuestAns;
        this.currentQuestionSet = list.data[0].QuestAns;
        this.ProductID = list.data[0].ProductID;
        // console.log("currentQuestionSet",this.currentQuestionSet);
      }else{
        this.spinner.hide();
        console.log(list);
      }
    });
  }

  overviewclickclick(){
    this.router.navigate(['report'], { queryParams: { id: this.id } });
  }
  overallperformanceclick(){
    this.router.navigate(['overallReport'], { queryParams: { id: this.id } });
  }

  downloadclick(){
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.downloadReport(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list);
        this.downloadreportqtns = list.Report;
     //   this.toastr.success('', list.message);
      }
    });
  }
  backarrowtoprojects(){
    this.router.navigate(['mocktestlist'], { queryParams: { id: this.ProductID } });
  }
  }

