import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  id: any; userAttempt: any;
  product: Product = new Product();
  user: User = new User();
  progress = Const.progress;
  apikey = Const.apikey;
  report: []; ProductID: any;
  performancereport: [];
  userData: any; name: any; email: any;
  userID: any; downloadoverviewreport: any;
  logoIcon = Images.logo_icon;
  usernameIcon = Images.logout_icon;
  reporttick = Images.reporttick;
  reportstar = Images.reportstar;
  reporttime = Images.reporttime;
  reportbluetick = Images.reportbluetick;
  testimonialsright = Images.testimonialsright;

  constructor(private toastr: ToastrService, private spinner: NgxSpinnerService, private locationStrategy: LocationStrategy, private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id = params.id;
        // console.log(this.id);
      });
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userID = this.userData[0].UserID;
    }
    this.mockreport();
    this.downloadclick();
    this.preventBackButton();
  }
  preventBackButton() {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
  }
  mockreport() {
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
     this.spinner.show();
    this.restservice.mockTestReport(this.product).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        this.spinner.hide();
        this.name = list.data[0].Name;
        this.email = list.data[0].EmailID;
        this.ProductID = list.data[0].ProductID;
        this.report = list.data;
      }
    });
  }

  questionsclick(attempt: any) {
    this.userAttempt = attempt;
    this.router.navigate(['questions'], { queryParams: { id: this.id, userAttempt: attempt  } });
  }
  overallperformanceclick(){
    this.router.navigate(['overallReport'], { queryParams: { id: this.id  } });
  }
  
  downloadclick() {
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
   // this.product['API-KEY'] = this.apikey;
    console.log(this.product);
    this.restservice.downloadReport(this.product).subscribe((list: any) => {
       //  console.log(list);
      if (list.status == 200) {
        this.downloadoverviewreport = list.Report;
        //this.toastr.success('', list.message);
      }
    });
  }

  backarrowtoprojects(){
    this.router.navigate(['mocktestlist'], { queryParams: { id: this.ProductID } });
  }
}
