import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Const } from 'src/app/constants/const';
import { Product } from 'src/app/models/product';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.css']
})
export class InstructionsComponent implements OnInit {
  id: any; userAttempt: any;
  product: Product = new Product();
  apikey = Const.apikey;
  mockTestInstructions:[];
  mockTestSubjects: [];
  mockTestInst: [];
  TotalTime: any;
  TotalQuestions:any; mocktesttype: any;
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  options = [
    {id: 1, name: 'English'},
  ]
  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id =params.id;
        this.userAttempt = params.userAttempt;
          // console.log(this.id, this.userAttempt);
         this.instructions();
      });
  }
  instructions(){
    this.product.MockTestID = this.id;
    this.product['API-KEY'] = this.apikey;
      this.spinner.show();
    this.restservice.mockTestDetailInstructions(this.product).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
        //  console.log(list);
          this.spinner.hide();
        this.TotalQuestions =list.data.TotalQuestions;
        this.mockTestInstructions =list.data;
        this.mocktesttype = list.data[0].MockTestType;
        this.TotalTime =list.data[0].TotalTime;
        this.mockTestSubjects =list.data[0].Subjects;
        this.mockTestInst =list.data[0].Instructions;
        //  console.log(list.data[0].Instructions);
        //  console.log(list.data[0].Subjects);
      }
    });
  }

  productid(id: any){
    // console.log(id);
    this.router.navigate( ['test'], {queryParams: { id: this.id, userAttempt: this.userAttempt } });
  }
}
