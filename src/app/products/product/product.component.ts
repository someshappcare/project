import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  courses: [];
  apikey = Const.apikey;
  progress =Const.progress;
  notsavedproduct = Images.notsavedproduct;
  savedproduct = Images.savedproduct;
  aboutuspic =Images.aboutus;
  emailfill = Images.emailfill;
  facebook2 = Images.facebook2;
  whatsapp2 = Images.whatsapp2;
  twitter2 = Images.twitter2;
   arrowwhiteright = Images.arrowwhiteright;
  products: [];
  productsclick: [];
  products1: any;
  path: any; 
  zero =Const.zero; one =Const.one; two = Const.two;
  userData: any; userid: any;
  product: Product = new Product();
  public selectedIndex: any; repoUrl: any;imageUrl: any;
  PageTitle: any; PageDescription: any; CourseDescription: any;
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService, private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    } else {
      this.userid = this.zero;
    }
    this.selectedIndex =this.zero;
   this.courseslist();
   this.productslist();
  }
  changeColor(i: any){
    this.selectedIndex = i;
  }
courseslist(){
  this.restservice.coursesList().subscribe((list: any) => {
    if (list.status == 200) {
      //  console.log("courses",list);
      this.courses = list.data;
      this.PageTitle = list.data[0].PageTitle;
      this.PageDescription = list.data[0].PageDescription;
      this.CourseDescription = list.data[0].CourseDescription;
      // console.log("CourseDescription",this.CourseDescription);
    }
  }, err => {
    console.log("error message" + err);
  });
}
productslist(){
  this.product.UserID = this.userid;
  this.product['API-KEY'] = this.apikey;
  //  console.log(this.product);
   this.spinner.show();
  this.restservice.productsList(this.product).subscribe((list: any) => {
    if (list.status == 200) {
       this.spinner.hide();
      // console.log(list.data[0].ProductImagePath);
     this.path = list.data[0].ProductImagePath;
      // console.log(list.data);
      this.products1 = list.data;
      this.productsclick =list.data;
      this.path = list.data[0].ProductImagePath;
      this.products = list.data[0].CourseProductList;
      // console.log(this.products);
    }
    
  });
}
  coursesclick(index: any) {
     this.spinner.show();
    this.restservice.productsList(this.product).subscribe((list: any) => {
      if (list.status == 200) {
         this.spinner.hide();
        //  console.log( list.data[index].CourseProductList);
        this.products = list.data[index].CourseProductList;
      }
      this.restservice.coursesList().subscribe((list: any) => {
        if (list.status == 200) {
          //  console.log("courses",list);
          this.courses = list.data;
          this.PageTitle = list.data[index].PageTitle;
          this.PageDescription = list.data[index].PageDescription;
          this.CourseDescription = list.data[index].CourseDescription;
          // console.log("CourseDescription",this.CourseDescription);
        }  });
    });
  }
  productid(id: any) {
    // console.log(id);
    this.router.navigate(['productDetails'], { queryParams: { id: id } });
  }
  saveproduct(save: any) {
    // console.log(save);
    this.product.UserID = this.userid;
    this.product.ProductID = save;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.saveProduct(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list.message);
        this.toastr.success('', list.message);
        this.productslist();
      }
    });
  }
  unsaveproduct(unsave: any) {
    // console.log(unsave);
    this.product.UserID = this.userid;
    this.product.ProductID = unsave;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.removeSavedProduct(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        this.toastr.success('', list.message);
        // console.log(list.message);
        this.productslist();
      }
    });
  }
  shredicons(){
    this.toastr.success('', this.progress); 
  }
}


