import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-overallperformance',
  templateUrl: './overallperformance.component.html',
  styleUrls: ['./overallperformance.component.css']
})
export class OverallperformanceComponent implements OnInit {
id: any; userAttempt: any; message: any;
product: Product = new Product();
progress = Const.progress;
apikey = Const.apikey;
report: []; ProductID: any;
performancereport: [];
userData: any;
userID: any; downloadoverviewreport: any;
logoIcon = Images.logo_icon;
  usernameIcon = Images.logout_icon;
  reporttick = Images.reporttick;
  reportstar = Images.reportstar;
  reporttime = Images.reporttime;
  testimonialsright = Images.testimonialsright;
constructor(private toastr: ToastrService, private spinner: NgxSpinnerService, private locationStrategy: LocationStrategy, private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }
  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id = params.id;
        // console.log(this.id);
      });
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userID = this.userData[0].UserID;
    }
    this.mocktestperformancereport();
    this.downloadclick();
  }

  mocktestperformancereport() {
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product['API-KEY'] = this.apikey;
    //  console.log(this.product);
    this.spinner.show();
    this.restservice.mockTestPerformanceReport(this.product).subscribe((list: any) => {
      //console.log(list);
      if (list.status == 200) {
         this.spinner.hide();
        // console.log(list.message);
        // console.log(list.data);
        this.ProductID = list.ProductID;
        this.performancereport = list.data;
      } else{
this.message = list.message;
      }
    });
  }

  // questionsclick() {
  //   this.router.navigate(['questions'], { queryParams: { id: this.id,userAttempt: this.userAttempt  } });
  // }
  overviewclick(){
    this.router.navigate(['report'], { queryParams: { id: this.id } });
  }

  downloadclick() {
    this.product.UserID = this.userID;
    this.product.MockTestID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.downloadReport(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list);
        this.downloadoverviewreport = list.Report;
      //  this.toastr.success('', list.message);
      }
    });
  }
  backarrowtoprojects(){
    this.router.navigate(['mocktestlist'], { queryParams: { id: this.ProductID } });
  }
}
