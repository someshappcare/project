import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverallperformanceComponent } from './overallperformance.component';

describe('OverallperformanceComponent', () => {
  let component: OverallperformanceComponent;
  let fixture: ComponentFixture<OverallperformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverallperformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverallperformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
