import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Const } from 'src/app/constants/const';
import { Product } from 'src/app/models/product';
import { RestapiService } from 'src/app/restapi.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-mocktestlist',
  templateUrl: './mocktestlist.component.html',
  styleUrls: ['./mocktestlist.component.css']
})
export class MocktestlistComponent implements OnInit {
  id: any; grandtest: any;
  product: Product = new Product();
  apikey = Const.apikey;
  noQuestions = Const.noQuestions;
  shareurl =Const.shareurl;
  zero = Const.zero;
  one = Const.one;
  two = Const.two;
  mocktests:[];
  userData: any;
  userID: any;productname: any;CourseName: any;
  constructor(private spinner: NgxSpinnerService, private toastr: ToastrService,private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id =params.id;
        //  console.log(this.id);
      });
      if (localStorage.getItem('currentUser')) {
        // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
        this.userData = JSON.parse(localStorage.getItem("currentUser"));
        this.userID = this.userData[0].UserID;
      }
      this.productdetailsbyid();
  }

  productdetailsbyid() {
    this.product.UserID = this.userID;
    this.product.ProductID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.productDetailsbyId(this.product).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        // console.log(list.data[0].GrandProduct);
        this.grandtest = list.data[0].GrandProduct;
        this.mocktestlist();
      }
    });
  }

  mocktestlist(){
    this.product.UserID = this.userID;
    this.product.ProductID = this.id;
    this.product['API-KEY'] = this.apikey;
    this.product.GrandProduct = this.grandtest;
    // console.log(this.product);
       this.spinner.show();
    this.restservice.mockTestList(this.product).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
       this.spinner.hide();
        // console.log(list);
        this.mocktests =list.data;
        this.productname = list.data[0].ProductName;
        this.CourseName =list.data[0].CourseName;
        //  console.log( this.mocktests);
      }
    });
  }
  mocktestid(mockid: any, questions: any, attempt: any){
    // console.log(mockid,questions);
    if(questions!=this.zero){
     this.router.navigate( ['instructions'], {queryParams: { id: mockid, userAttempt: attempt} });
    }else{
      this.toastr.success('', this.noQuestions);
    }
  }
  mocktestid1(mockid: any, attempt: any){
    // console.log(mockid, questions);
    this.router.navigate( ['test'], {queryParams: { id: mockid, userAttempt: attempt, } });
  }
  mocktestid2(mockid: any){
    // console.log(mockid,questions);
    this.router.navigate( ['report'], {queryParams: { id:mockid  } });
  }
    mocktestid3(mockid: any, questions: any, attempt: any){
     if(questions!=this.zero){
     this.router.navigate( ['instructions'], {queryParams: { id: mockid, userAttempt: attempt } });
    }else{
      this.toastr.success('', this.noQuestions);
    }
}

}
