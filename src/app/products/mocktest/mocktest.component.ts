import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';
import { RestapiService } from 'src/app/restapi.service';
import {formatDate,Location} from '@angular/common';

@Component({
  selector: 'app-mocktest',
  templateUrl: './mocktest.component.html',
  styleUrls: ['./mocktest.component.css']
})
export class MocktestComponent implements OnInit {
  id: any;
  product: Product = new Product();
  apikey = Const.apikey;
  mocktestsideimage = Images.mocktestsideimage;
  mocktestnumber = Images.mocktestnumber;
  mocktestshare = Images.mocktestshare;
  logintotketest = Const.logintotketest;
  logintopayment = Const.logintopayment;
  nomocktesttobuy = Const.nomocktesttobuy;
  buytotketest = Const.buytotketest;
  noSlotsvailabledata = Const.noSlotsvailabledata;
  sloticon =Images.sloticon;
  zero = Const.zero;
  one = Const.one;
 currentdateandslotSet: any;
  plus = Images.plus;
  userData: any;
  userID: any; UserBookSlot: any;
  path: any; selectedtimeslotid:any; selectedtimeslot:any; 
   myDate: any; hours:any; minitues:any; day: any; date:any; totalpresenttimeinmin: any; currentdate: any;
  sessionsleftarrow =Images.sessionsleftarrow;
  sessionsrightarrow = Images.sessionsrightarrow;
  progress = Const.progress; SelectedDatefromdb: any; TimeSlotfromdb: any;
  UserPurchased: any; selectedslotIndex:any;
  prodcutdetils: []; datewithslots: []; datewithslotslength:any; nocurrentslots:any;
  mocktestCount: any; name: any; desc: any;image: any;
  faqlist: []; selecteddayIndex: any; selecteddatetopply: any;
  @ViewChild('closebokkslotModel') closebokkslotModel: any;
  @ViewChild('openbokkslotModel') openbokkslotModel: any;
  @ViewChild('closeshareModel') closeshareModel: any;

  currentIndex: any; displaycurrentslots =[];
  
  constructor(private location: Location, private spinner: NgxSpinnerService, private toastr: ToastrService, private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { 
    this.location.subscribe(location => {
      this.closebokkslotModel.nativeElement.click();
      this.closeshareModel.nativeElement.click();
    });
  }
  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      //  console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userID = this.userData[0].UserID;
    } else {
      this.userID = 0;
    }
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id = params.id;
        // console.log(this.id);
        this.productdetailsbyid();
        this.faq();
      });
  }
  back(): void {
    this.router.navigate(['products']);
  }
  productdetailsbyid() {
    this.selectedslotIndex = null;
    this.currentIndex = 0; 
    this.displaycurrentslots =[];
    this.hours =  new Date().getHours();
    this.date =new Date();
    this.myDate = formatDate(this.date, 'yyyy-MM-dd', 'en-US');
    this.currentdate = new Date(this.myDate);
    this.day = this.currentdate.getDay();
    this.selecteddayIndex =this.day;
    // console.log("timedate", this.hours, this.minitues, this.totalpresenttimeinmin, this.myDate, this.day);

    this.product.UserID = this.userID;
    this.product.ProductID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.spinner.show();
    this.restservice.productDetailsbyId(this.product).subscribe((list: any) => {
    // console.log(list);
      this.spinner.hide();
      if (list.status == 200) {
        // console.log(list.message);
        this.path = list.ProductImagePath;
        this.UserPurchased = list.UserPurchased;
        this.mocktestCount = list.CountMockTest;
        this.UserBookSlot = list.UserBookSlot;
        this.prodcutdetils = list.data;
        if(list.SlotData.length >this.zero){
        this.SelectedDatefromdb = list.SlotData[0].SelectedDate;
        this.TimeSlotfromdb = list.SlotData[0].TimeSlot;
        }
        this.datewithslots = list.data[this.currentIndex].AvailDays;
        this.currentdateandslotSet =this.datewithslots[this.currentIndex];
        this.datewithslotslength = this.datewithslots.length-1;
        // this.nocurrentslots = this.currentdateandslotSet?.TimeSlots.length;
        this.selecteddatetopply = this.currentdateandslotSet?.Date;
        for (var i = 0; i < this.currentdateandslotSet?.TimeSlots.length; i++) {
          if(this.hours < this.currentdateandslotSet?.TimeSlots[i].StartTime && this.myDate==this.selecteddatetopply){
            this.displaycurrentslots.push(this.currentdateandslotSet?.TimeSlots[i]);
        } 
      }
        this.nocurrentslots = this.displaycurrentslots.length;
        // console.log("datewithslots", this.datewithslots, this.currentdateandslotSet, this.datewithslotslength, this.nocurrentslots, this.displaycurrentslots, this.displaycurrentslots.length);
      }
    });
  }

  productid() {
    if (this.mocktestCount != this.zero && this.userID != this.zero && this.UserPurchased != this.zero) {
      this.router.navigate(['mocktestlist'], { queryParams: { id: this.id } });
    } else if (this.userID != this.zero && this.UserPurchased == this.zero && this.mocktestCount != this.zero) {
      this.toastr.error('', this.buytotketest);
    } else if (this.userID == this.zero) {
      this.toastr.error('', this.logintotketest);
    } else if (this.userID != this.zero && this.mocktestCount == this.zero) {
      this.toastr.error('', this.nomocktesttobuy);
    }
  }
  faq() {
    this.restservice.faq().subscribe((list: any) => {
      if (list.status == 200) {
        //  console.log(list);
        this.faqlist = list.data;
        //  console.log(this.faqlist);
      }
    }, err => {
      console.log("error message" + err);
    });
  }
  navpaymentpage() {
    if (this.userID != this.zero && this.mocktestCount != this.zero) {
      this.router.navigate(['payment'], { queryParams: { id: this.id } });
    } else if (this.mocktestCount == this.zero && this.userID != this.zero) {
      this.toastr.error('', this.nomocktesttobuy);
    }
    else {
      this.toastr.error('', this.logintopayment);
    }
  }

  navlivepaymentpage(){
    if (this.userID != this.zero) {
      this.router.navigate(['payment'], { queryParams: { id: this.id } });
    }
    else {
      this.toastr.error('', this.logintopayment);
    }
  }
  sharedetails(list: any){
    // console.log(list);
    this.name = list.ProductName;
    this.desc = list.ShortDescription;
    this.image = this.path + list.ProductImage;
  }

  openbookingslotmodel(){
  // this.selectedslotIndex = null;
  if (this.userID != this.zero) {
  this.openbokkslotModel.nativeElement.click();
  }else{
    this.toastr.error('', this.logintotketest);
  }
}

  dateleftclick(){
    if(this.currentIndex >0){
    this.displaycurrentslots =[];
    this.currentIndex = this.currentIndex - 1;
    this.currentdateandslotSet = this.datewithslots[this.currentIndex];
    this.selectedslotIndex = null;
    this.selecteddatetopply = this.currentdateandslotSet?.Date;
    this.currentdate = new Date(this.currentdateandslotSet?.Date);
    this.day = this.currentdate.getDay();
    this.selecteddayIndex = this.day;
    // this.nocurrentslots = this.currentdateandslotSet?.TimeSlots.length;
    for (var i = 0; i < this.currentdateandslotSet?.TimeSlots.length; i++) {
      if(this.myDate!=this.currentdateandslotSet?.Date){
        this.displaycurrentslots.push(this.currentdateandslotSet?.TimeSlots[i]);
    }else if(this.hours < this.currentdateandslotSet?.TimeSlots[i].StartTime && this.myDate==this.selecteddatetopply){
      this.displaycurrentslots.push(this.currentdateandslotSet?.TimeSlots[i]);
  } 
  }
  this.nocurrentslots = this.displaycurrentslots.length;
    // console.log(this.currentIndex,this.currentdateandslotSet, this.currentdate, this.day, this.nocurrentslots, this.displaycurrentslots);
  }
}
  daterightclick(){
    if(this.currentIndex <this.datewithslotslength){
    this.displaycurrentslots =[];
    this.currentIndex = this.currentIndex + 1;
    this.currentdateandslotSet = this.datewithslots[this.currentIndex];
    this.selectedslotIndex = null;
    this.selecteddatetopply = this.currentdateandslotSet?.Date;
    this.currentdate = new Date(this.currentdateandslotSet?.Date);
    this.day = this.currentdate.getDay();
    this.selecteddayIndex = this.day;
    //this.nocurrentslots = this.currentdateandslotSet?.TimeSlots.length;
    for (var i = 0; i < this.currentdateandslotSet?.TimeSlots.length; i++) {
      if(this.myDate!=this.currentdateandslotSet?.Date){
        this.displaycurrentslots.push(this.currentdateandslotSet?.TimeSlots[i]);
    }
  }
   this.nocurrentslots = this.displaycurrentslots.length;
    // console.log(this.currentIndex, this.datewithslotslength, this.currentdateandslotSet, this.currentdate, this.day,this.nocurrentslots), this.displaycurrentslots;
  }
}
changeslotColor(i:any, TimeSlot: any,TimeSlotID: any ){
  this.selectedslotIndex = i;
  this.selectedtimeslotid =TimeSlotID;
  this.selectedtimeslot = TimeSlot;
}
slotbookingapplybutton(){
  this.product.UserID = this.userID;
  this.product.ProductID = this.id;
  this.product.TimeSlotID = this.selectedtimeslotid;
  this.product.TimeSlot = this.selectedtimeslot;
  this.product.SelectedDate = this.selecteddatetopply;
  this.product['API-KEY'] = this.apikey;
 // console.log("slotbookingapplybutton", this.product);
  this.spinner.show();
  this.restservice.bookSlot(this.product).subscribe((list: any) => {
      // console.log(list);
    if (list.status == 200) {
      this.spinner.hide();
       this.closebokkslotModel.nativeElement.click();
      // this.redirectTo('productDetails');
      this.productdetailsbyid();
      this.toastr.success('', list.message);
    }
  });
}

// redirectTo(uri:string){
//   this.router.navigateByUrl('productDetails', {skipLocationChange: true}).then(()=>
//   this.router.navigate([uri]));
// }

daystodisplay: any[] = [
  { "name": "Sun", id: 0 },
  { "name": "Mon", id: 1 },
  { "name": "Tue", id: 2 },
  { "name": "Wed", id: 3 },
  { "name": "Thu", id: 4 },
  { "name": "Fri", id: 5 },
  { "name": "Sat", id: 6 },
];

}

