import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-updateprofiledetails',
  templateUrl: './updateprofiledetails.component.html',
  styleUrls: ['./updateprofiledetails.component.css']
})
export class UpdateprofiledetailsComponent implements OnInit {
  userData: any;
  username: any;
  email: any;
  mobileno: any;
  userid: any;
  detailsbyid: any;
  userdetails: [];
  submitted = false;
  error ="";
  updateuserprofileform:FormGroup;
  usernameValidation = Const.usernames_validation;
  mobilenoValidation = Const.mobileno_validation;
  mobilenolengthValidation = Const.mobilenolength_validation;
  mobilenodigitsValidation = Const.mobilenodigits_validation;
  user: User = new User();
  apikey = Const.apikey;
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService,private formBuilder: FormBuilder,private router: Router, private restservice: RestapiService) { }
  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
      this.userdetailslist();
    }
  }
  get f() { return this.updateuserprofileform.controls; }

  userdetailslist() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
     this.spinner.show();
    this.restservice.userDetailsById(this.user).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
         this.spinner.hide();
        // console.log("success");
        // this.SpinnerService.hide();
        this.username = list.data[0].Name;
        this.email = list.data[0].EmailID;
        this.mobileno = list.data[0].MobileNumber;

        this.updateuserprofileform = this.formBuilder.group({
          fullname: [this.username, [Validators.required]],
          mobile: [this.mobileno, [Validators.required, Validators.pattern("^[0-9]*$")]],
        });
      }
    });
  }

  onSubmit() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
    this.user.Mobile = this.updateuserprofileform.value.mobile;
    this.user.Name = this.updateuserprofileform.value.fullname;
    this.submitted = true;
    // console.log(this.updateuserprofileform.value);
    if (this.updateuserprofileform.invalid) {
      return;
    }
     this.spinner.show();
    this.restservice.UserProfileUpdate(this.user).subscribe((list: any) => {
      if (list.status == 200) {
         this.spinner.hide();
        // console.log(list.message);
       this.toastr.success('', list.message);
        this.router.navigate(['/profile']);
      } else {
        // console.log(list.message);
         this.spinner.hide();
       this.toastr.error('', list.message);
      }
    }, err => {
      console.log("error message");
    });
  }
  }

