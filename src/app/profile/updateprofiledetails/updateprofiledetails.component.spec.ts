import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateprofiledetailsComponent } from './updateprofiledetails.component';

describe('UpdateprofiledetailsComponent', () => {
  let component: UpdateprofiledetailsComponent;
  let fixture: ComponentFixture<UpdateprofiledetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateprofiledetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateprofiledetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
