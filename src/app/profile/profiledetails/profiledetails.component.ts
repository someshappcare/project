import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Const } from 'src/app/constants/const';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';
import { Images } from 'src/app/constants/images';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-profiledetails',
  templateUrl: './profiledetails.component.html',
  styleUrls: ['./profiledetails.component.css']
})
export class ProfiledetailsComponent implements OnInit {

  userData: any;
  username: any;
  email: any;
  mobileno: any;
  userid: any;
  educationForm: FormGroup;
  detailsbyid: any;
  submitted = false;
  resumeurl: any;
  selectfile: any;
  url: any;
  error: any;
  FilePath: any;
  ResumeFileName: any;
  userdetails: any =[];
  usereducationdetails: [];
  ugprofiledegreelst: [];
  pgprofiledegreelst: [];
  selectedDegreeId: any;
  educationDetailslength: any;
  educationrowvisible: any;
  specializationById: any; getyear: any;
  degreeValidation = Const.degree_validation;
  specilizationValidation = Const.specilization_validation;
  startyearValidation = Const.startyear_validation;
  endyearValidation = Const.endyear_validation;
  endyearstartyearValidation = Const.endyearstartyear_validation;
  specilizationpatternvalidation = Const.specilization_pattern_validation
  notsavedproduct = Images.notsavedproduct;
  cross = Images.cross;
  user: User = new User();
  apikey = Const.apikey;  
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService,private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService) { }
  startYear =[];
  endYear=[];
  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    this.getyear = new Date().getFullYear();
    //console.log("this.getyear",this.getyear);
      this.userdetailslist();
      this.profiledegreelistdata();
      this.loopcondition();
    }
    this.educationForm = this.formBuilder.group({
      ugdegree: ['', [Validators.required]],
      ugspecilization: ['', [Validators.required, Validators.pattern('[a-zA-Z0-9]*')]],
      ugstartyear: ['', [Validators.required]],
      ugendyear: ['', [Validators.required]],
      pgdegree: [''],
      pgspecilization: [''],
      pgstartyear: [''],
      pgendyear: ['']
    });
  }

  get f() { return this.educationForm.controls; }

  userdetailslist() {
    this.user.UserID =this.userid;
    this.user['API-KEY'] = this.apikey;
    this.spinner.show();
    this.restservice.userDetailsById(this.user).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        this.spinner.hide();
        // console.log(list.data,list.educationDetails);
        this.FilePath =list.FilePath;
        this.usereducationdetails = list.educationDetails;
        // console.log(list.educationDetails.length);
        this.educationDetailslength =list.educationDetails.length;
        this.username = list.data[0].Name;
        this.email = list.data[0].EmailID;
        this.mobileno = list.data[0].MobileNumber;
        this.ResumeFileName =list.data[0].ResumeFileName;
      }
    });
  }
  profiledegreelistdata() {
    this.restservice.profileDegreesList().subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
        //  console.log(list);
        this.ugprofiledegreelst = list.UGDegreeList;
        this.pgprofiledegreelst = list.PGDegreeList;
      }
    });
  }

  eduSubmit() {
    this.submitted = true;
    // console.log(this.educationForm.value);
    if (this.educationForm.invalid) {
      // console.log(this.educationForm.invalid);
      // console.log("return");
      return;
    }
    if (this.educationForm.value.ugstartyear > this.educationForm.value.ugendyear) {
      this.toastr.error('', this.endyearstartyearValidation);
      // console.log(this.error);
    }else{
      // console.log("else");
    this.user.UserID = this.userid;
    this.user.UGDegreeID = this.educationForm.value.ugdegree,
      this.user.UGSpecialization = this.educationForm.value.ugspecilization,
      this.user.UGStartYear = this.educationForm.value.ugstartyear,
      this.user.UGEndYear = this.educationForm.value.ugendyear,
      this.user.PGDegreeID = this.educationForm.value.pgdegree,
      this.user.PGSpecialization = this.educationForm.value.pgspecilization,
      this.user.PGStartYear = this.educationForm.value.pgstartyear,
      this.user.PGEndYear = this.educationForm.value.pgendyear,
      this.user['API-KEY'] = this.apikey;
      // console.log(this.user);
      this.spinner.show();
    this.restservice.saveEducationDetails(this.user).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
      //  console.log(list.message);
       this.spinner.hide();
       this.toastr.success('', list.message);
       this.submitted = false;
       this.educationForm.reset();
       this.userdetailslist();
      }
    });
  }
}
onresumeSelectFile(event) {
    // console.log("onSelectFile");
    this.selectfile = event.target.files && event.target.files[0];
    if (this.selectfile) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectfile);
      // console.log(this.selectfile, this.selectfile.name, this.selectfile.type);
      this.resumeurl = this.selectfile;
    }
    this.uploadresumeindb();
  }
  uploadresumeindb() {
    // console.log("upload resume");
    const formData = new FormData();
    formData.append('UserID', this.userid);
    formData.append('file', this.resumeurl);
    formData.append('API-KEY', this.apikey);
    // console.log(formData);
    // console.log("before api");
    // console.log(this.userid, this.resumeurl, this.apikey);
    this.spinner.show();
    this.restservice.userResumeUpload(formData).subscribe((list: any) => {
      // console.log(list.status);
      this.spinner.hide();
      // console.log(list.message);
      this.toastr.success('', list.message);
      this.userdetailslist();
    });
  }
  
  loopcondition(){
    for (var j = 2011; j <= this.getyear; j++) {
        this.startYear.push(j);
        this.endYear.push(j);
    } 
  }

}
