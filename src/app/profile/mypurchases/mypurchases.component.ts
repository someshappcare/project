import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-mypurchases',
  templateUrl: './mypurchases.component.html',
  styleUrls: ['./mypurchases.component.css']
})
export class MypurchasesComponent implements OnInit {
  userData: any;
  userid: any;
  apikey = Const.apikey;
  arrow =Images.arrow;
  arrowup =Images.arrowup;
  rupee = Images.rupee;
  path: any;
  zero = Const.zero;
  one =Const.one;
  purchases: [];
  isToggled = false;
   public selectedIndex: any; message: any;
  paymentstat1: any; paymentstat2: any;
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }
  user: User = new User();
  public selectedIndex1: any;
  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    }
    this.selectedIndex =this.zero;
    this.paymentstat1 = "Paid";this.paymentstat2 = "Failed";
    this.purchaselistdata();
  }
  changeColor(i: any){
    this.selectedIndex = i;
  }
  purchaselistdata() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
    // console.log(this.user);
       this.spinner.show();
    this.restservice.purchaseProduct(this.user).subscribe((list: any) => {
      if (list.status == 200) {
           this.spinner.hide();
        // console.log(list.message);
         // console.log("purchaselistdata",list);
         this.path =list.ProductImagePath;
         this.purchases =list.data;
       // console.log(list.data);
      }else{
        this.spinner.hide();
        console.log(list);
        this.message = list.message;
      }
    });
  }
  purchasesclick(paymentstatus1: any, paymentstatus2: any){
    // console.log(paymentstatus1,paymentstatus2);
    this.paymentstat1 =paymentstatus1;
    this.paymentstat2 =paymentstatus2;
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
     this.spinner.show();
   this.restservice.purchaseProduct(this.user).subscribe((list: any) => {
    if (list.status == 200) {
       this.spinner.hide();
     // console.log("purchasesclick",list);
      this.purchases =list.data;
    }else{
        this.spinner.hide();
       // console.log(list);
        this.message = list.message;
      }
    });
  }
  purchasestype = [
    { title: 'All Purchases', paymentstatus1: "Paid", paymentstatus2: "Failed" },
    { title: 'Paid', paymentstatus1: "Paid", paymentstatus2: "Paid" },
    { title: 'Fail', paymentstatus1: "Failed", paymentstatus2: "Failed" }
  ]

  onToggle(value: any, id: any) {
    // console.log(value, id);
      this.selectedIndex1 = value;
      this.isToggled = !this.isToggled;
}
  productid(id: any) {
    // console.log(id);
    this.router.navigate(['productDetails'], { queryParams: { id: id } });
  }
}
