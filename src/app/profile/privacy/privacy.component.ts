import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  privacylist: [];
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.privcyistdata();
  }
  privcyistdata(){
    this.spinner.show();
    this.restservice.privacyPolicy().subscribe((list: any) => {
     if (list.status == 200) {
      //  console.log(list);
       this.spinner.hide();
       this.privacylist = list.data;
      //  console.log(this.privacylist);
     }
   }, err => {
     console.log("error message" + err);
   });
   }

}
