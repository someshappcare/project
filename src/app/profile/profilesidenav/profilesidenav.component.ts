import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-profilesidenav',
  templateUrl: './profilesidenav.component.html',
  styleUrls: ['./profilesidenav.component.css']
})
export class ProfilesidenavComponent implements OnInit {
  userData: any;
  username: any;
  privacy =Images.privacy;
  allcourses =Images.allcourses;
  purchases =Images.purchases;
  profilesetting =Images.profilesetting;
  profileicon = Images.profileicon;
  FilePath: any;
  key =Images.key;
  selectedfile: any;
  user: User = new User();
  format: any;
  fileimage: any;
  profileimage: any;
  apikey = Const.apikey;
  url: any;
  userid: any;
  logout =Images.logout;
  help =Images.help;
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService,public router: Router,private restservice: RestapiService) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
     //  console.log("get local storage navbar" +  localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.username = this.userData[0].EmailID;
      this.userid = this.userData[0].UserID;
      this.userdetailslist();
    }
  }
  userdetailslist() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
    this.restservice.userDetailsById(this.user).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        // console.log("success");
        this.FilePath =list.FilePath;
        this.profileimage =list.data[0].ProfileImageName;
        // console.log( this.FilePath, list.data[0].ProfileImageName);
      }
    });
  }
  onSelectFile(event) {
    this.selectedfile = event.target.files && event.target.files[0];
    if (this.selectedfile) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedfile);
      // console.log(this.selectedfile, this.selectedfile.name, this.selectedfile.type);
      if (this.selectedfile.type.indexOf("image") > -1) {
        this.format = "image";
        this.fileimage = this.selectedfile;
      }
      reader.onload = event => {
        this.url = (<FileReader>event.target).result;
      };
    }
    this.uploadprofilepic();
  }

  uploadprofilepic() {
    const formData = new FormData();
    formData.append('UserID', this.userid);
    formData.append('file', this.fileimage);
    formData.append('API-KEY', this.apikey);
    this.spinner.show();
    this.restservice.profilephotoUpdate(formData).subscribe((list: any) => {
      // console.log(list.message);
      this.spinner.hide();
      // console.log(list);
      this.toastr.success('', list.message);
      this.userdetailslist();
    });
  }
  logoutclick(){
    // console.log("removed user");
    localStorage.removeItem('currentUser')
    // location.reload();
    this.router.navigateByUrl('/');
  } 
}
