import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-allcourses',
  templateUrl: './allcourses.component.html',
  styleUrls: ['./allcourses.component.css']
})
export class AllcoursesComponent implements OnInit {
  userData: any;
  userid: any;
  user: User = new User();
  apikey = Const.apikey;
  notsavedproduct = Images.notsavedproduct;
  savedproduct = Images.savedproduct;
  SavedCourses: [];
  ProductImagePath: any;
  message: any;
  img = 'assets/mba-resume-writing-1.png';
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    } 
    this.savedcourseslist();
  }
  savedcourseslist(){
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
    //  console.log(this.user);
     this.spinner.show();
    this.restservice.savedProductList(this.user).subscribe((list: any) => {
      if (list.status == 200) {
         this.spinner.hide();
        this.ProductImagePath = list.ProductImagePath;
        // console.log(list);
        // console.log(list.data);
        // console.log(list.data);
        this.SavedCourses = list.data;
      }else{
        this.spinner.hide();
        // console.log(list.message);
        this.message = list.message;
      }
    });
  }
  savedcoursesclick(id: any){
    this.router.navigate(['productDetails'], { queryParams: { id: id } });
  }
}
