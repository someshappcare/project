import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Passwordmatcher } from 'src/app/constants/passwordmatcher';
import { Changepwd } from 'src/app/models/changepwd';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  submitted = false;
  submitt =false;
  changePasswordData: any;
  error = '';
  userData: any;
  userID: any;
  apikey =Const.apikey;
  fieldTextTypePassword: any;
  confirm_text_password: any;
  new_confirm_text_password: any;
  passwordIcon = Images.password_icon;
  passwordValidation = Const.password_validation;
  confirmpasswordValidation = Const.confirmpassword_validation;
  oldpasswordValidation = Const.oldpassword_validation;
  newpasswordValidation = Const.newpassword_validation;
  confirmpasswordvalidatorValidation = Const.confirmpasswordvalidator_validation;
  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  toggleConfirmFieldPassword() {
    this.confirm_text_password = !this.confirm_text_password;
  }

  newtoggleConfirmFieldPassword() {
    this.new_confirm_text_password = !this.new_confirm_text_password;
  }
  changepwd : Changepwd = new Changepwd();
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService, private formBuilder: FormBuilder, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group({
      oldpassword: ['', [Validators.required]],
      newpassword: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]]
    }, {
      validator: Passwordmatcher('newpassword', 'confirm_password')
    });
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userID = this.userData[0].UserID;
    }
  }
  get f() { return this.changePasswordForm.controls; }
  onSubmit() {
    this.changepwd.UserID =this.userID,
    this.changepwd.oldPassword = this.changePasswordForm.value.oldpassword,
    this.changepwd.newPassword = this.changePasswordForm.value.newpassword,
    this.changepwd.confirmPassword = this.changePasswordForm.value.newpassword,
    this.changepwd['API-KEY'] =this.apikey
    
    this.submitted = true;
     //console.log(this.changepwd);
    if (this.changePasswordForm.invalid) {
      return;
    }
    this.spinner.show();
    this.restservice.changePassword(this.changepwd).subscribe((list: any) => {
      if (list.status == 200) {
        this.spinner.hide();
         this.toastr.success('', list.message);
         this.submitted = false;
         this.changePasswordForm.reset();
      } else {
        // console.log(list.message);
        this.spinner.hide();
        this.toastr.error('', list.message);
        this.submitted = false;
        this.changePasswordForm.reset();
      }
    }, err => {
      console.log("error message");
    });
  }
  }
