import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-refunds',
  templateUrl: './refunds.component.html',
  styleUrls: ['./refunds.component.css']
})
export class RefundsComponent implements OnInit {

  refundcancellation: [];
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.refundcancellationdata();
  }
  refundcancellationdata(){
    this.spinner.show();
    this.restservice.refundPolicy().subscribe((list: any) => {
     if (list.status == 200) {
      // console.log(list);
       this.spinner.hide();
       this.refundcancellation = list.data;
     //  console.log(this.refundcancellation);
     }
   }, err => {
     console.log("error message" + err);
   });
   }

}