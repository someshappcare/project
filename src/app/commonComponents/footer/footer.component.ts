import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { RestapiService } from 'src/app/restapi.service';
import { Images } from 'src/app/constants/images';
import { Product } from 'src/app/models/product';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  userData: any;
  userid: any;
  progress =Const.progress;
  logintoseefooter = Const.logintoseefooter;
  product: Product = new Product();
  apikey = Const.apikey;
  clientmobile = Const.clientmobile;
  clientemail = Const.clientemail;
  logo_icon = Images.logo_icon2;
  phonecall = Images.phonecall;
  facebook2 =Images.facebook2;
  whatsapp2 =Images.whatsapp2;
  twitter2 =Images.twitter2;
  mail = Images.mail;
  topsessions: [];
  constructor(private toastr: ToastrService,private router: Router,private restservice: RestapiService) { }

  ngOnInit(): void {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage footer" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    } else {
      this.userid = "0";
    }
    this.userTopSessions();
  }
  userTopSessions(){
    this.product.UserID = this.userid;
    this.product['API-KEY'] = this.apikey;
    this.restservice.userTopSessions(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list);
        this.topsessions = list.data;
        // console.log(this.topsessions);
      }
    }, err => {
      console.log("error message" + err);
    });
  }
  
  aboutusfooter(){
    this.router.navigate(['/aboutUs']);
  }
  termsfooter(){
  this.router.navigate(['/termsandconditions']);
  }
  refundfooter(){
  this.router.navigate(['/refundsandcancellation']);
}
privcyfooter(){
  this.router.navigate(['/privacypolicy']);
}
  mbafooter(id: any){
    this.router.navigate(['productDetails'], { queryParams: { id: id } });
  }
  subscribeemail(){
  }
}
