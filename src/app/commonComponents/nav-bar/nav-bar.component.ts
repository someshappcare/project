import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  isLoggedIn: boolean;
  userData: any;
  username: any;
  email: any;
  user: User = new User();
  profileimage: any;
  apikey = Const.apikey;
  url: any;
  userid: any;
  FilePath: any;
  logoIcon = Images.logo_icon;
  searchIcon = Images.search_icon;
  progress =Const.progress;
  notificationIcon = Images.notification_icon;
  notificationDropDownIcon = Images.notification_dropdown_icon;
  logoutIcon = Images.logout_icon;
  profileIcon = Images.profile_icon;
  constructor(private toastr: ToastrService,private router: Router,private restservice: RestapiService) { }

  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.username = this.userData[0].Username;
      this.userid = this.userData[0].UserID;
      this.email = this.userData[0].EmailID;
      this.isLoggedIn = true;
      this.userdetailslist();
    }
  }
  userdetailslist() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
    this.restservice.userDetailsById(this.user).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        // console.log("navbarsuccess");
        this.FilePath =list.FilePath;
        // console.log(list.data[0].ProfileImageName);
        this.profileimage =list.data[0].ProfileImageName;
        // console.log( this.FilePath, list.data[0].ProfileImageName);
      }
    });
  }
  searchclick(){
    this.toastr.error('', this.progress);
  }
  logoutclick() {
    // console.log("removed user");
    localStorage.removeItem('currentUser');
    // location.reload();
    this.isLoggedIn = false;
     this.router.navigateByUrl('/');
  }
}
