import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RestapiService } from 'src/app/restapi.service';
import { Product } from 'src/app/models/product';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  productData: any;
  sessionsData: any;
  private fragment: string;
  apikey = Const.apikey;
  notsavedproduct = Images.notsavedproduct;
  savedproduct = Images.savedproduct;
  testimonialsleft = Images.testimonialsleft;
  testimonialsright = Images.testimonialsright;
  arrowright = Images.arrowright;
  subscribe1 =Images.subscribe1;
  arrowleft = Images.arrowleft;
  arrowwhiteright = Images.arrowwhiteright;
  star = Images.star;
  starcolor = Images.starcolor;
  rightarrowcarousal = Images.rightarrowcarousal;
  leftarrowcarousal = Images.leftarrowcarousal;
  sessionsleftarrow = Images.sessionsleftarrow;
  sessionsrightarrow = Images.sessionsrightarrow;
  userData: any;
  userid: any;
  path: any;
  featurespath: any;
  aboutuslist: any;
  product: Product = new Product();
  products: [];
  featureslist: [];
  topSessions: [];
  zero =Const.zero; one =Const.one; two = Const.two;
  testimonialslist: [];howweworklist: [];howweworkimagepath: any;
  testimonialslistloop: any;
  subscribed =Const.subscribed; homestatarray: [];
  homeStatsmessage: any; ImagePathhomestat: any;
  constructor(private locationStrategy: LocationStrategy,private spinner: NgxSpinnerService,private toastr: ToastrService,private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }
  chunks(array: any, size: any) {
    let results = [];
    results = [];
    while (array.length) {
      results.push(array.splice(0, size));
    }
    return results;
  }
  ngOnInit() {
    if (localStorage.getItem('currentUser')) {
      // console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
    } else {
      this.userid = this.zero;
    }
    this.spinner.show();
    this.mbaproducts();
    this.sessions();
    this.featuresservice();
    this.aboutuslistdata();
    this.testimonialsdata();
    this.howWeWork();
    this.homeStats();
    this.preventBackButton();
    this.spinner.hide();
  }
  preventBackButton() {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
  }
  mbaproducts(){
    this.product.UserID = this.userid;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.productsList(this.product).subscribe((list: any) => {
      // console.log(list.data);
      this.path = list.data[0].ProductImagePath;
      // console.log(list.data[0].CourseProductList);
      this.products = list.data[0].CourseProductList;
      this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
      this.productData = this.chunks(this.products, 3);
      let last = this.productData[this.productData.length - 1].length;
      if (this.productData.length > 1 && last < 3) {
        this.productData[this.productData.length - 1] = [...this.productData[this.productData.length - 1], ...this.productData[0].slice(0, 3 - last)];
      }
    })
  }
  sessions(){
     this.product.UserID = this.userid;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.userTopSessions(this.product).subscribe((list: any) => {
      // console.log("sessions",list.data);
      this.topSessions = list.data;
      this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
    this.sessionsData = this.chunks(this.topSessions, 4);
    let last1 = this.sessionsData[this.sessionsData.length - 1].length;
    if (this.sessionsData.length > 1 && last1 < 4) {
      this.sessionsData[this.sessionsData.length - 1] = [...this.sessionsData[this.sessionsData.length - 1], ...this.sessionsData[0].slice(0, 4 - last1)];
    }
    })
  }
  testimonialsdata(){
     this.restservice.testimonial().subscribe((list: any) => {
         if (list.status == 200) {
          //  console.log(list);
           this.testimonialslist = list.data;
          //  console.log(this.testimonialslist);
            this.route.fragment.subscribe(fragment => { this.fragment = fragment; });
          this.testimonialslistloop = this.chunks(this.testimonialslist,3);
          let last2 = this.testimonialslistloop[this.testimonialslistloop.length-1].length;
          if(this.testimonialslistloop.length > 1 && last2 < 3) {
            this.testimonialslistloop[this.testimonialslistloop.length-1] = [ ...this.testimonialslistloop[this.testimonialslistloop.length-1] , ...this.testimonialslistloop[0].slice(0, 3-last2) ];
          }
         }
       }, err => {
         console.log("error message" + err);
       });
  }
  
  ngAfterViewInit(): void {
    try {
      document.querySelector('#' + this.fragment).scrollIntoView();
    } catch (e) {
      console.log(e);
    }
  }
  productid(id: any) {
    // console.log(id);
    this.router.navigate(['productDetails'], { queryParams: { id: id } });
  }
  saveproduct(save: any) {
    // console.log(save);
    this.product.UserID = this.userid;
    this.product.ProductID = save;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.saveProduct(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list.message);
        this.toastr.success('', list.message);
        this.mbaproducts();
      }
    });
  }
  unsaveproduct(unsave: any) {
    // console.log(unsave);
    this.product.UserID = this.userid;
    this.product.ProductID = unsave;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.removeSavedProduct(this.product).subscribe((list: any) => {
      if (list.status == 200) {
        // console.log(list.message);
        this.toastr.success('', list.message);
        this.mbaproducts();
      }
    });
  }

  featuresservice(){
     this.restservice.featureServices().subscribe((list: any) => {
         if (list.status == 200) {
          //  console.log(list);
           this.featurespath =list.FeatureImagePath;
           this.featureslist = list.data;
          //  console.log(this.featureslist);
         }
       }, err => {
         console.log("error message" + err);
       });
  }
  homeStats(){
    this.restservice.homeStats().subscribe((list: any) => {
      if (list.status == 200) {
       // console.log(list);
        this.homestatarray = list.data;
        this.ImagePathhomestat =list.ImagePath;
        this.homeStatsmessage = list.message;
      }
    }, err => {
      console.log("error message" + err);
    });
  }

 aboutuslistdata(){
        this.restservice.aboutUs().subscribe((list: any) => {
         if (list.status == 200) {
          //  console.log(list);
           this.aboutuslist = list.data;
          //  console.log(this.aboutuslist);
         }
       }, err => {
         console.log("error message" + err);
       });
       }

 subscribe(){
   if(this.userid==0){
    this.router.navigate(['login']);
   }else{
 this.toastr.success('',this.subscribed);
   }
 }

getstartedclick(){
 this.router.navigate(['/products']);
}

 howWeWork(){
  this.restservice.howWeWork().subscribe((list: any) => {
    if (list.status == 200) {
    //  console.log(list);
      this.howweworkimagepath =list.ImagePath;
      this.howweworklist = list.data;
    }
  }, err => {
    console.log("error message" + err);
  });
 }
}
