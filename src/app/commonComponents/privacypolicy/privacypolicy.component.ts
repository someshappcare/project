import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-privacypolicy',
  templateUrl: './privacypolicy.component.html',
  styleUrls: ['./privacypolicy.component.css']
})
export class PrivacypolicyComponent implements OnInit {

   privacypolicy: [];
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.privacyPolicydata();
  }
  privacyPolicydata(){
    this.spinner.show();
    this.restservice.privacyPolicy().subscribe((list: any) => {
     if (list.status == 200) {
     //  console.log(list);
       this.spinner.hide();
       this.privacypolicy = list.data;
      //  console.log(this.termsconditions);
     }
   }, err => {
     console.log("error message" + err);
   });
   }

}