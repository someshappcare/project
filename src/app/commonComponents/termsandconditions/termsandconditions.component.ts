import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { RestapiService } from 'src/app/restapi.service';

@Component({
  selector: 'app-termsandconditions',
  templateUrl: './termsandconditions.component.html',
  styleUrls: ['./termsandconditions.component.css']
})
export class TermsandconditionsComponent implements OnInit {

   termsconditions: [];
  constructor(private spinner: NgxSpinnerService,private router: Router, private restservice: RestapiService) { }

  ngOnInit(): void {
    this.termsdata();
  }
  termsdata(){
    this.spinner.show();
    this.restservice.termsAndConditions().subscribe((list: any) => {
     if (list.status == 200) {
     //  console.log(list);
       this.spinner.hide();
       this.termsconditions = list.data;
      //  console.log(this.termsconditions);
     }
   }, err => {
     console.log("error message" + err);
   });
   }

}