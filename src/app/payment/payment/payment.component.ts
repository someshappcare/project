import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Const } from 'src/app/constants/const';
import { Images } from 'src/app/constants/images';
import { Order } from 'src/app/models/order';
import { Product } from 'src/app/models/product';
import { User } from 'src/app/models/user';
import { RestapiService } from 'src/app/restapi.service';
import { WindowRefService } from 'src/app/window-ref.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {
  userData: any;
  userid: any;
  user: User = new User();
  product: Product = new Product();
  order: Order = new Order();
  apikey = Const.apikey;
  logo_icon = Images.logo_icon;
  logoIcon = Images.logo_icon;
  delete = Images.delete;
  dotlines =Images.dotlines;
  rupee = Images.rupee;
  username: any;
  email: any;
  mobileno: any;
  id: any;
  path: any;
  razorkey: any;
  prodcutdetils: [];
  Amount: any;ProductName: any;ShortDescription: any;
  creteorderform: any; sendorderdetailsform: any;
  responsepaymentid: any; responseorderid: any; responsesignature: any;
  proOrderId: any;
  constructor(private spinner: NgxSpinnerService,private toastr: ToastrService,private zone: NgZone, private winRef: WindowRefService, private router: Router, private restservice: RestapiService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.id =params.id;
        //  console.log(this.id);
      });
    if (localStorage.getItem('currentUser')) {
    //  console.log("get local storage navbar" + localStorage.getItem('currentUser'));
      this.userData = JSON.parse(localStorage.getItem("currentUser"));
      this.userid = this.userData[0].UserID;
      this.razorkey = this.restservice.razorPayKey();
      this.userdetailslist();
      this.productdetailsbyid();
    } 
  }
  userdetailslist() {
    this.user.UserID = this.userid;
    this.user['API-KEY'] = this.apikey;
       this.spinner.show();
    this.restservice.userDetailsById(this.user).subscribe((list: any) => {
      // console.log(list.status);
      if (list.status == 200) {
        // console.log("success");
        this.username = list.data[0].Name;
        this.email = list.data[0].EmailID;
        this.mobileno = list.data[0].MobileNumber;
      }
    });
  }
  productdetailsbyid() {
    this.product.UserID = this.userid;
    this.product.ProductID = this.id;
    this.product['API-KEY'] = this.apikey;
    // console.log(this.product);
    this.restservice.productDetailsbyId(this.product).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
         this.spinner.hide();
        // console.log(list.message);
        this.path = list.ProductImagePath;
        this.prodcutdetils =list.data;
        this.Amount = Math.floor((list.data[0].Amount) * 100);
        this.ProductName = list.data[0].ProductName;
        this.ShortDescription = list.data[0].ShortDescription;
        //  console.log(list.data[0].ProductName);
      }
    });
  }
  createRzpayOrder() {
    this.creteorderapifrombackend();
  }
  creteorderapifrombackend(){
    this.order.UserID= this.userid;
    this.order.ProductID= this.id;
    this.order.Amount= this.Amount;
    this.order['API-KEY']= this.apikey;
    // console.log(this.order);
    this.restservice.createOrderId(this.order).subscribe((list: any) => {
      // console.log(list);
      if (list.status == 200) {
        this.proOrderId = list.orderId;
        // console.log(list.message);
        // console.log(list.orderId, this.proOrderId);
         this.payWithRazor();
      }
    });
  }
  payWithRazor() {
    // console.log("payWithRazor", this.proOrderId);
    const options: any = {
      key: this.razorkey,
      amount: this.Amount, // amount should be in paise format to display Rs 1255 without decimal point
      currency: 'INR',
      name: 'Treta', // company name or product name
      description: this.ProductName,  // product description
      image: this.logoIcon, // company logo or product image
      order_id: this.proOrderId, // order_id created by you in backend
      modal: {
        // We should prevent closing of the form when esc key is pressed.
        escape: false,
      },
      notes: {
        // include notes if any
      },
      theme: {
        color: '#0c238a'
      }
    };
    options.handler = ((response: any, error) => {
      options.response = response;
      console.log(response);
      this.responsepaymentid =response.razorpay_payment_id;
      this.responseorderid =response.razorpay_order_id;
      this.responsesignature =response.razorpay_signature;
      // console.log(options);
      console.log(this.responsepaymentid,this.responseorderid,this.responsesignature);
      this.sendorderdetailstobackend();
      // call your backend api to verify payment signature & capture transaction
    });
    options.modal.ondismiss = (() => {
      // handle the case when user closes the form while transaction is in progress
      // console.log('Transaction cancelled.');
      this.toastr.error('', 'Transaction cancelled');
    });
    const rzp = new this.winRef.nativeWindow.Razorpay(options);
       console.log("Failed");
    rzp.open();
  }
  sendorderdetailstobackend(){
    this.order.PaymentID= this.responsepaymentid;
    this.order.OrderID= this.responseorderid;
    this.order.Signature= this.responsesignature;
    this.order.Amount= this.Amount;
    this.order['API-KEY']= this.apikey;
    // console.log(this.order);
       this.spinner.show();
    this.restservice.verifyOrderResponse(this.order).subscribe((list: any) => {
      // console.log(list);
         this.spinner.hide();
      this.toastr.success('', list.message);
        console.log(list.message);
        this.zone.run(() => {
          this.router.navigate( ['productDetails'], {queryParams: { id: this.id } });
      });
    });
  }
  }
