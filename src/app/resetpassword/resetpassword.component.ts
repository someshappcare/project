import { LocationStrategy } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Const } from '../constants/const';
import { Images } from '../constants/images';
import { Passwordmatcher } from '../constants/passwordmatcher';
import { Changepwd } from '../models/changepwd';
import { User } from '../models/user';
import { RestapiService } from '../restapi.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  left_img = Images.left_image;
  logoIcon = Images.logo_icon;
  passwordIcon = Images.password_icon;
  passwordValidation = Const.password_validation;
  confirmpasswordValidation = Const.confirmpassword_validation;
  confirmpasswordvalidatorValidation = Const.confirmpasswordvalidator_validation;
  resetForm: FormGroup;
  submitted = false;
  fieldTextTypePassword: any;
  confirm_text_password: any;
  changepwd: Changepwd = new Changepwd();
  apikey =Const.apikey;
  email: any;
  constructor(private route: ActivatedRoute, private locationStrategy: LocationStrategy,private spinner: NgxSpinnerService, private toastr: ToastrService,private router: Router, private formBuilder: FormBuilder, private restservice: RestapiService) { }
  toggleFieldPassword() {
    this.fieldTextTypePassword = !this.fieldTextTypePassword;
  }

  toggleConfirmFieldPassword() {
    this.confirm_text_password = !this.confirm_text_password;
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe(
      params => {
        // console.log(params);
        this.email =params.em;
        //  console.log(this.email);
      });
    this.resetForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      confirm_password: ['', [Validators.required]]
    }
    , {
      validator: Passwordmatcher('password', 'confirm_password')
    });
    this.preventBackButton();
  }
  get f() { return this.resetForm.controls; }
  preventBackButton() {
    history.pushState(null, null, location.href);
    this.locationStrategy.onPopState(() => {
      history.pushState(null, null, location.href);
    })
  }
  onresetSubmit(){
    this.submitted = true;
    this.changepwd.newPassword =this.resetForm.value.password,
    this.changepwd.confirmPassword =this.resetForm.value.confirm_password,
    this.changepwd.Email =this.email,
    this.changepwd['API-KEY'] =this.apikey
    // console.log("form");
    // console.log(this.resetForm.value);
    // console.log(this.changepwd);
    if (this.resetForm.invalid) {
      // console.log(this.resetForm.invalid);
      // console.log("return");
      return;
    }else{
      this.spinner.show();
      // console.log(this.changepwd);
      this.restservice.resetPassword(this.changepwd).subscribe((list: any) => {
        if (list.status == 200) {
          // console.log(list.message);
          this.spinner.hide();
          this.router.navigate(['login']);
        } else {
          // console.log(list.message);
          this.spinner.hide();
          this.toastr.error('', list.message);
          this.submitted = false;
          this.resetForm.reset();
        }
      }, err => {
        console.log("error message");
      });
    }
  }
}
