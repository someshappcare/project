import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './about-us/about-us.component';
import { HomeComponent } from './commonComponents/home/home.component';
import { NavBarComponent } from './commonComponents/nav-bar/nav-bar.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LoginComponent } from './login/login.component';
import { MocktestComponent } from './products/mocktest/mocktest.component';
import { MocktestlistComponent } from './products/mocktestlist/mocktestlist.component';
import { PaymentComponent } from './payment/payment/payment.component';
import { ProductComponent } from './products/product/product.component';
import { RegisterComponent } from './register/register.component';
import { ProfiledetailsComponent } from './profile/profiledetails/profiledetails.component';
import { UpdateprofiledetailsComponent } from './profile/updateprofiledetails/updateprofiledetails.component';
import { ChangepasswordComponent } from './profile/changepassword/changepassword.component';
import { AllcoursesComponent } from './profile/allcourses/allcourses.component';
import { MypurchasesComponent } from './profile/mypurchases/mypurchases.component';
import { InstructionsComponent } from './products/instructions/instructions.component';
import { TestComponent } from './products/test/test.component';
import { ReportsComponent } from './products/reports/reports.component';
import { PrivacyComponent } from './profile/privacy/privacy.component';
import { TermsComponent } from './profile/terms/terms.component';
import { AuthguardService } from './constants/authguard.service';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { QuestionsComponent } from './products/questions/questions.component';
import { OverallperformanceComponent } from './products/overallperformance/overallperformance.component';
import { RefundsComponent } from './commonComponents/refunds/refunds.component';
import { TermsandconditionsComponent } from './commonComponents/termsandconditions/termsandconditions.component';
import { PrivacypolicyComponent } from './commonComponents/privacypolicy/privacypolicy.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'prefix'},
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'resetpassword', component: ResetpasswordComponent},
  { path: 'navBar', component: NavBarComponent },
  { path: 'aboutUs', component: AboutUsComponent },
  { path: 'contactUs', component: ContactUsComponent },
  { path: 'products', component: ProductComponent },
  { path: 'productDetails', component: MocktestComponent },
  { path: 'mocktestlist', component: MocktestlistComponent},
  { path: 'payment', component: PaymentComponent },
  { path: 'profile', component: ProfiledetailsComponent, canActivate: [AuthguardService] },
  { path: 'update', component: UpdateprofiledetailsComponent, canActivate: [AuthguardService] },
  { path: 'changePassword', component: ChangepasswordComponent, canActivate: [AuthguardService]},
  { path: 'allcourses', component: AllcoursesComponent, canActivate: [AuthguardService] },
  { path: 'mypurchases', component: MypurchasesComponent, canActivate: [AuthguardService] },
  { path: 'instructions', component: InstructionsComponent },
  { path: 'test', component: TestComponent},
  { path: 'report', component: ReportsComponent},
  { path: 'questions', component: QuestionsComponent},
  { path: 'overallReport', component: OverallperformanceComponent},
  { path: 'refundsandcancellation', component: RefundsComponent},
  { path: 'termsandconditions', component: TermsandconditionsComponent},
  { path: 'privacypolicy', component: PrivacypolicyComponent},
  { path: 'privacy', component: PrivacyComponent, canActivate: [AuthguardService]},
  { path: 'terms', component: TermsComponent, canActivate: [AuthguardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled', useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
